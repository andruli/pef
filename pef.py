#! /usr/bin/env python3
#coding=utf-8

if __name__ == '__main__':
    import importlib
    import traceback
    import contracts
    import argparse
    import proxy
    import _builtins  # This line must be below the proxy import statement.
    import time
    import sys
    try:
        from clint.textui import colored
    except ImportError:
        print("`clint` module not found, please run `pip3 install clint`.")
        sys.exit(-1)
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("file_and_function", action="store",
                        help="Syntax: <program_file>:<function_name>")
    parser.add_argument("-d", "--depth", action="store", default=10,
                        help="Depth limit")
    parser.add_argument("-D", "--debug", action="store_true",
                        help="Show more info")
    parser.add_argument("-s", "--show-output", action="store_true",
                        help="Run tests")
    parsed_args = parser.parse_args()
    module_name, function_name = parsed_args.file_and_function.split(':')
    module = importlib.import_module(module_name.strip().split(".")[0])
    function = getattr(module, function_name.strip())
    # It seams to be necessary to save and restore the builtins attribute
    # before exiting, so we save them now
    original_bultins =  module.__builtins__
    module.__builtins__ = _builtins
    results = []
    try:
        # Lazy Explore (results are yield in a iterator)
        results = proxy.explore(function, max_depth=int(parsed_args.depth))
    except contracts.ContractIsNotValidExpressionError as error:
        exc_name, exc_msg = repr(error).split('(', 1)
        print("\n{}: {}\n".format(colored.red(exc_name), exc_msg[1:-2]))
        sys.exit()
    executed = 0
    fully_explored = 0
    max_depth_reached = 0
    start_time = time.process_time()
    for result in results:
        args = result['args']
        kwargs = result['kwargs']
        ret_value = result['result']
        exception = result['exception']
        outputs = result['outputs']
        path = result['path']
        pathcondition = result['pathcondition']
        executed += 1
        max_depth_reached = max(max_depth_reached, len(pathcondition))
        if isinstance(exception, proxy.MaxDepthError):
            # If we reached max depth, continue with the next exploration
            continue
        fully_explored += 1
        # Making args, kwargs and ret_value strings for printing
        args = [repr(arg) for arg in args]  # args needs to be strings
        kwargs = [str(k) + '=' + repr(v) for k, v in kwargs.items()]
        function_signature = "{f}({args})".format(f=function.__name__,
                                                  args=', '.join(args + kwargs))
        if not exception or isinstance(exception, contracts.ContractError):
            function_signature += colored.magenta(' -> ') + repr(ret_value)
        print(function_signature)
        if outputs:  # Print all the stdout writes made in `function'
            print(outputs)
        if exception:
            exception_text = repr(exception).split('(')
            exception_name = exception_text.pop(0)
            exception_message = "".join(exception_text)[:-1]  # Removing last )
            validation_msg = ""
            if hasattr(exception, '_is_valid') and exception._is_valid:
                validation_msg = "[Valid by contract]"
            exception_pprint = "\t{name}({message}) {validation_msg}".format(\
                                name=colored.red(exception_name),
                                message=exception_message,
                                validation_msg=colored.green(validation_msg))
            print(exception_pprint)
            if parsed_args.debug:
                print("\n\t".join(traceback.format_tb(x[3].__traceback__)))
    elapsed_time = (time.process_time() - start_time)
    # Restoring original builtins reference in module
    module.__builtins__ = original_bultins
    # Making final report
    print("\nExploring time: {:.2f} seconds.".format(elapsed_time))
    print("{0} paths found with exploration depth of {1}, "\
           "using {2} as depth limit.".format(executed, max_depth_reached,
                                              parsed_args.depth))
    coverage_status = colored.green("Complete")
    if fully_explored < executed:
        not_explored = executed - fully_explored
        print("{0} paths were fully explored. {1} were aborted.".format(fully_explored,
                                                              not_explored))
        coverage_status = colored.red("Incomplete")
    print("\nPath exploration coverage status: {}".format(coverage_status))
