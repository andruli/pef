#! /usr/bin/env python3
#coding=utf-8

import argparse, importlib, proxy, psutil, os, sys, time, threading, signal,\
       queue, subprocess, pickle
try:
    import psutil
except:
    print("`psutil` module not found, please run `pip3 install psutil`")

class ProcessMonitor(threading.Thread):
    """
    This class wraps a process and keeps track of the cpu use, the memory
    and the memory
    """
    def __init__(self, process, output_queue, tbr=1):
        threading.Thread.__init__(self)

        self.tbr = tbr
        self.process = process
        self.oqueue = output_queue
        self.setDaemon(1)
        self.start()

    def run(self):
        result = dict(user_time=0, sys_time=0, mem=0, v_ctxswt=0, i_ctxswt=0)
        while True:
            try:
                # Run until the program stops
                if not self.process.is_running():
                    break
                # result['mem'].append(self.process.get_memory_info().rss)
                result['user_time'] = self.process.get_cpu_times().user
                result['sys_time'] = self.process.get_cpu_times().system
                result['memory'] = max(result['mem'], self.process.get_memory_info().rss)
                result['vcs'] = self.process.get_num_ctx_switches().voluntary
                result['ics'] = self.process.get_num_ctx_switches().involuntary
                time.sleep(self.tbr)
            except psutil.NoSuchProcess:
                pass
            except psutil.AccessDenied:
                pass
            self.oqueue.put(result)

def benchmark(program, time_between_readings=0.00001):
    """
    Benchmark an external program.
    Returns a dictionary containing user/sys time, max_memory used and
    voluntary/involuntary context switches.
    """
    result = queue.Queue()
    try:
        process = psutil.Popen(program, stdout=subprocess.DEVNULL)
        pmonitor = ProcessMonitor(process, result, time_between_readings)
        process.wait()
        return result.get()
    except KeyboardInterrupt:
        os.kill(process.pid, signal.SIGKILL)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("file_and_function", action="store",
                        help="Syntax: <program_file>:<function_name>")
    parser.add_argument("-d", "--depth", action="store", default=10,
                        help="Depth limit")
    parser.add_argument("-n", "--nexp", action="store", default=5,
                        help="Depth limit")

    import _builtins
    parsed_args = parser.parse_args()
    module_name, function_name = parsed_args.file_and_function.split(':')
    module = importlib.import_module(module_name.strip().split(".")[0])
    function = getattr(module, function_name.strip())
    original_bultins =  module.__builtins__
    module.__builtins__ = _builtins
    proxy.ProxyObject.max_depth = int(parsed_args.depth)
    # Compute first the inputs needed to concrete execute
    print("Precomputando los valores necesarios...")
    r = proxy.explore(function)
    inputs = []
    for x in r: inputs.append(x[0])
    # Save the data
    data_file_name = "/tmp/pef_benchmark_dump.txt"
    pickle.dump(inputs, open(data_file_name, "wb"))
    i = pickle.load(open(data_file_name, "rb"))
    # Create auxiliar python code
    python_aux_exe_file_name = "/tmp/pef_benchmark_real_exe.py"
    real_repeat_times = 200
    open(python_aux_exe_file_name, "wt").write("""
import pickle, importlib, sys
sys.path.append('%s')
from %s import %s as fun
input = pickle.load(open('%s', 'rb'))
for x in range(%d):
    for x in input:
        try:
            fun(*x)
        except:
            pass
"""% (os.getcwd(), module_name[:module_name.rindex(".")], function_name, data_file_name, real_repeat_times))
    # Result Storage
    pef  = dict(user_time=0, sys_time=0, memory=0, vcs=0, ics=0)
    real = pef.copy()
    # Command line call
    pef_args = [sys.argv[0][:sys.argv[0].rindex("/")+1] + "pef.py"]
    pef_args.append("-d"); pef_args.append(str(parsed_args.depth))
    pef_args.append(parsed_args.file_and_function)
    real_args = ["python3", python_aux_exe_file_name]
    # Begin experiments
    print("Comenzando la repetición de experimentos")
    exp = int(parsed_args.nexp)
    for i in range(exp):
        print("\tEjecutando experimento %d/%s..." % (i+1,exp))
        # Call Pef Executable
        pef_benchmark = benchmark(pef_args)
        for x in pef: pef[x] += pef_benchmark[x]
        # Call the real deal
        real_benchmark = benchmark(real_args)
        for x in real: real[x] += real_benchmark[x]
    for x in pef:
        pef[x]  /= exp
        real[x] /= (exp * real_repeat_times)

    print("-" * 79)
    print("|" + ("Resultados al ejecutar %s %d veces" % (function_name, exp)).center(77) + "|")
    print("-" * 79)
    print("|" + "Experimento".center(25) + "|" + "PEF".center(25) + "|" + "Real".center(25) + "|")
    print("-" * 79)
    for x in pef:
        print("|" + x.center(25) + "|" + str(pef[x]).center(25) + "|" + str(real[x]).center(25) + "|")
        print("-" * 79)
    # Restore builtins
    module.__builtins__ = original_bultins