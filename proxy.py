#coding:utf-8
import types as _types
import contracts
import copy
import re
from collections import Iterable
from ast import literal_eval
import itertools
from _builtins import range, len, enumerate
from capture import Capturing
from proxy_decorators import *
# Import objets for the SMT Solver
from smt.smt import SMT, SMTException
from smt.sort_z3 import SMTInt, SMTBool, SMTChar, SMTArray
from smt.solver_z3 import SMTSolver

# Gideline para crear/modificar proxys:
#   *) Si queremos saber si una variable tiene algo preguntar si es != None.
#      Comparaciones con valores o usar como booleanos a variables puede
#      producir branching no deseado.
#   *) La implementación de __str__ y __repr__ de los Proxy no debe generar
#      branching. Facilita el debugeo y evita caminos generalmente innecesarios.
#   *) No utilizar la función `len` ya que python hace checkeo de tipos.
#   *) Los métodos _concretize deben devolver un objeto concreto "equivalente".
#   *) Cada proxy object DEBE tener un atributo de clase `emulated_class` que
#      tenga como valor la clase que el proxy object emula. ej.
#      ProxyInt.emulated_class = int, ProxyList.emulated_class = list

smt = SMT((SMTInt, SMTBool, SMTChar, SMTArray), SMTSolver)

class ConcolicExecutionError(Exception):
    pass


class MaxDepthError(ConcolicExecutionError):
    def __init__(self):
        super(MaxDepthError, self).__init__("Max depth reached")


def is_builtin(obj):
    if obj == None:
        return True
    if type(obj) != type:
        return is_builtin(type(obj))
    else:
        return obj.__name__ in __builtins__


def _concretize(self, model):
    #FIXME: Hay que iterar sobre builtins que puedan contener ProxyObjects
    if isinstance(self, ProxyObject):
        return self._concretize(model)
    elif isinstance(self, list):
        return [_concretize(x, model) for x in self]
    elif isinstance(self, tuple):
        return tuple([_concretize(x, model) for x in self])
    elif is_builtin(self):
        return self
    elif callable(self):
        return self
    else:
        # User defined (abstract) class
        # self.__dict__ returns all instance-only defined attributes
        try:
            for name in self.__dict__:
                # TODO: Hacerlo genérico, incluyendo atributos de clase
                obj = getattr(self, name)
                if not callable(obj):
                    setattr(self, name, _concretize(obj, model))
        except AttributeError:
            from collections import Iterable
            if isinstance(self, Iterable):
                pass

        return self

def _boolsify(obj):
    """
    Iterates over obj attributes and call bool() on each one.
    This is used to force branching during exploration when the result object
    contains bool operations unevaluated because of lazy evaluation.

    UPDATE: This function it's not necesary, as it turns out resulted objects that
    contain unresolved symbolic booleans don't miss any path exploration that
    gives any useful information.
    """
    for name in dir(obj):
        bool(getattr(obj, name))
        # FIXME: hacer la parte recursiva


def proxify(obj):
    """
    Transforms object :obj: into a proxy class object
    """
    if isinstance(obj, ProxyObject) or isinstance(obj, type(None)):
        return obj
    if isinstance(obj, type) and issubclass(obj, ProxyObject):
        return obj()
    # Dynamic generation of this table allow adding new proxy classes at run time
    real_to_proxy = {x.emulated_class: x for x in ProxyObject.__subclasses__()}
    instance = None if isinstance(obj, type) else obj
    instance_type = type(obj) if instance is not None else obj
    assert(hasattr(instance_type, '__name__'))
    if instance_type.__name__ in __builtins__:
        # instance_type is builtin, we just return the corresponding Proxy
        # Object instance
        if instance is not None:
            return real_to_proxy[instance_type](instance)
        else:
            return real_to_proxy[instance_type]()
    elif instance_type == type(None):
        return None
    else:
        # instance_type is a user_defined class
        tc = contracts.TypeContract.parse(instance_type.__init__)
        args_count = obj.__init__.__code__.co_argcount
        if not tc and args_count > 1:
            raise contracts.ContractError("Arguments without contracts declared.")
        elif args_count == 1:
            args, kwargs = [], {}
        else:
            args, kwargs = tc.types['args'], tc.types['kwargs']
            args = args[1:] # Removing the self reference.
        for i, arg in enumerate(args):
            assert(arg)
            # Get `randomly` some type, and try them all!
            args[i] = proxify(_branch_iterate(arg))
        for k, v in kwargs.items():
            kwargs[k] = proxify(_branch_iterate(v))
        result = instance_type(*args, **kwargs)
        result._concretize = _types.MethodType(_concretize, result)
        return result
    #TODO: soporte para kwargs


def _branch_iterate(iterable):
    """
    yields objects in iterable creating a new branch for each.
    """
    ip = IntProxy()
    index = None
    for i in range(len(iterable) - 1):
        if ip < i:
            index = i
            break
    index = len(iterable) - 1 if index is None else index
    return iterable[index]


def get_args_kwargs_permutations(args, kwargs):
    """
    `kwargs': {key: ([type1_of_argument, type2_of_argument, ],  default_value)}
    """
    args_as_lists = [[x] if not isinstance(x, list) else x for x in args]
    kwargs_as_lists = [[{k:v} for v in v] for k, v in kwargs.items()]
    args_combinations = list(itertools.product(*args_as_lists))
    kwargs_combinations = list(itertools.product(*kwargs_as_lists))
    for args in args_combinations:
        for kwargs_list in kwargs_combinations:
            kwargs = {}
            for d in kwargs_list:  # [{kwargs1: value1}, {kwarg2: value2}]
                assert(len(d) == 1)
                kwargs.update({k:v for k, v in d.items()})
            yield (args, kwargs)


def _concretize_result(self, model):
    if isinstance(self, dict):
        for k,v in self.items():
            self[k] =_concretize_result(v, model)
        return self
    else:
        return _concretize(self, model)


def explore(function, max_depth=None, args=None, kwargs=None):
    """
    If `args' or `kwargs' are not given, ask types from function contract.
    """
    if not (args or kwargs):
        con = contracts.find_all_contracts(function)
        types = con[contracts.TypeContract].types
        args, kwargs = types['args'], types['kwargs']
    for args, kwargs in get_args_kwargs_permutations(args, kwargs):
        for r in ProxyObject.explore(function, args, kwargs, con, max_depth):
            yield _concretize_result(r, r['model'])


class ProxyObject(object):
    """
    Base class of a ProxyObject that can become any type by inheriting it.
    """
    _path = []  # Desitions we made on every branch we could choose
    _pathcondition = []  # _pathcondition[i] = constrains until branch i
    _facts = []  # Other branch-independent constrains, like preconditions
    max_depth = 10
    # XXX: pensar en varios threads haciendo ejecuciones a la vez

    @classmethod
    def explore(cls, function, args=None, kwargs=None, con=None, max_depth=None):
        """
        Explore all reachable paths of function `function` using symbolic
        execution with a depth limit of `max_depth'.

        `*args' must contain the type of the original arguments of `function'.

        If arguments of `function' can have multiple types, user must call
        this function several times, a call for each posible permutation of the
        types.

        `con': contracts
        """
        if max_depth:
            ProxyObject.max_depth = max_depth
        cls._path = []
        have_paths_to_explore = True
        # args += (varargs, )
        # kwargs.update(varkwargs)
        con = contracts.find_all_contracts(function) if not con else con
        while have_paths_to_explore:
            cls._pathcondition = []
            cls._facts = []
            result = {'args': None, 'kwargs': None, 'result': None,
                      'exception': None, 'outputs': None, 'path': None,
                      'pathcondition': None, 'model': None}
            try:
                pargs = [proxify(a) for a in args] if args else []
                if con[contracts.TypeContract].types['varargs']:
                    pargs += [x for x in ListProxy()]
                pkwargs = {k: proxify(v) for k, v in kwargs.items()} if kwargs else {}
            except MaxDepthError as e:
                result['exception'] = e
                result['path'] = copy.copy(cls._path)  # Copy is for future threading
                result['pathcondition'] = copy.copy(cls._pathcondition)
                result['model'] = smt.get_model(cls._pathcondition + cls._facts)
                yield(result)
            else:
                nargs = copy.deepcopy(pargs)  # Copy for execution
                nkwargs = copy.deepcopy(pkwargs)  # Copy for execution
                iargs = copy.deepcopy(pargs)  # Copy to know initial values
                ikwargs = copy.deepcopy(pkwargs)  # Copy to know initial values
                exception = None
                returnv = None
                pruned = False
                output = ""
                try:
                    # TODO: varkwargs
                    with Capturing() as output:  # Capture stdout writes
                        # Validate precondition
                        if con[contracts.AssumeContract]:
                            con[contracts.AssumeContract].validate(*nargs,
                                                                **nkwargs)
                        returnv = function(*nargs, **nkwargs)
                        # Validate postcondition
                        if con[contracts.EnsureContract]:
                            con[contracts.EnsureContract].validate(returnv,
                                                        *iargs, **ikwargs)
                except SMTException as e:
                    raise e
                except contracts.PreConditionError:
                    pruned = True
                except Exception as e:
                    exception = e
                    # if we have exception contracts. Check if this is a error?
                    exception_contract = con[contracts.ExceptionContract]
                    if not isinstance(e, contracts.ContractError) and \
                                      exception_contract:
                        exception._is_valid = exception_contract.validate(e,
                                            *nargs, **nkwargs)
                finally:
                    if pruned:
                        break
                    result['model'] = smt.get_model(cls._pathcondition + cls._facts)
                    result['args'] = iargs
                    result['kwargs'] = ikwargs
                    result['result'] = returnv
                    result['exception'] = exception
                    result['path'] = cls._path
                    result['pathcondition'] = cls._pathcondition
                    result['facts'] = cls._facts
                    yield(result)
            # Set the next exploration path:
            # 1 Remove all False elements at the end of branch
            #   (those branches have been explored already)
            path = cls._path  # path it's shorter
            while len(path) > 0 and not path[-1]:
                path.pop()
            # 2.1 If path is empty, the whole branch tree has been explored
            #     (with MaxDepth limit)
            if not path:
                have_paths_to_explore = False
            # 2.2 Else, switch the last true branch to false to explore
            #     another path
            else:
                path[-1] = False

    def _concretize(self, model):
        """
        Given a model, create the real value the proxy object is representing
        """
        raise NotImplementedError()


class IntProxy(ProxyObject):
    """
    Int Proxy object for variables that behave like ints.
    """
    emulated_class = int

    def __init__(self, real=None):
        """
        :real: Either symbolic numeric variable in smt solver or real int value.
        """
        if isinstance(real, IntProxy):
            self.real = real.real  # Idempotent creation
        elif real != None:
            self.real = real
        else:
            self.real = smt.Const(SMTInt)

    def __abs__(self):
        """
        x.__abs__() <==> abs(x)
        """
        return self if self > 0 else -self

    @forward_to_rfun()
    def __add__(self, other):
        """
        :types: other: int
        x.__add__(y) <==> x+y
        """
        return IntProxy(smt.Add(self.real, other.real))

    def __bool__(self):
        """
        x.__bool__() <==> x != 0
        """
        return bool(self != 0)

    def __deepcopy__(self, memo):
        """
        x.__deepcopy__() <==> copy.deepcopy(x)
        """
        return self

    @check_equality
    def __eq__(self, other):
        """
        :types: other: int
        x.__eq__(y) <==> x==y
        """
        other = other.real if isinstance(other, IntProxy) else other
        return BoolProxy(smt.Eq(self.real, other))

    @forward_to_rfun()
    def __floordiv__(self, other):
        """
        :types: other: int
        x.__floordiv__(y) <==> x//y
        """
        if other != 0:
            # Fix z3 bugs
            # 1) -1 == -1//2 != 1//-2  == 0
            # 2)  1 ==  1//2 != -1//-2 == 0
            if other < 0:
                return -self//-other
            else:
                return IntProxy(smt.Div(self.real, other.real))
        else:
            raise ZeroDivisionError("IntProxy division or modulo by zero")

    @check_comparable_types
    def __ge__(self, other):
        """
        :types: other: int
        x.__ge__(y) <==> x>=y
        """
        other = other.real if isinstance(other, IntProxy) else other
        return BoolProxy(smt.Ge(self.real, other))

    @check_comparable_types
    def __gt__(self, other):
        """
        :types: other: int
        x.__gt__(y) <==> x>y
        """
        other = other.real if isinstance(other, IntProxy) else other
        return BoolProxy(smt.Gt(self.real, other))

    def __hash__(self):
        """
        x.__hash__() <==> hash(x)
        """
        return super().__hash__()

    @check_comparable_types
    def __le__(self, other):
        """
        :types: other: int
        x.__le__(y) <==> x<=y
        """
        other = other.real if isinstance(other, IntProxy) else other
        return BoolProxy(smt.Le(self.real, other))

    @check_comparable_types
    def __lt__(self, other):
        """
        :types: other: int
        x.__lt__(y) <==> x<y
        """
        other = other.real if isinstance(other, IntProxy) else other
        return BoolProxy(smt.Lt(self.real, other))

    @forward_to_rfun()
    def __mod__(self, other):
        """
        :types: other: int
        x.__mod__(y) <==> x%y
        """
        if other != 0:
            return IntProxy(smt.Mod(self.real, other.real))
        else:
            raise ZeroDivisionError("IntProxy division or modulo by zero")

    @forward_to_rfun()
    def __mul__(self, other):
        """
        :types: other: int
        x.__mul__(y) <==> x*y
        """
        return IntProxy(smt.Mul(self.real, other.real))

    def __ne__(self, other):
        """
        :types: other: int
        x.__ne__(y) <==> x!=y
        """
        other = other.real if isinstance(other, IntProxy) else other
        return BoolProxy(smt.Not(smt.Eq(self.real, other)))

    def __neg__(self):
        """
        x.__neg__() <==> -x
        """
        return IntProxy(smt.Neg(self.real))

    @check_self_and_other_have_same_type
    def __radd__(self, other):
        """
        :types: other: int
        x.__radd__(y) <==> y+x
        """
        return self.__add__(other)

    def __repr__(self):
        """
        x.__repr__() <==> repr(x)
        """
        if not isinstance(self.real, int):
            s = smt.simplify(self.real)
            if SMTInt.isSMTValue(s):
                s = SMTInt.concreteValue(s)
        return "IntProxy(%s)" % s

    @check_self_and_other_have_same_type
    def __rfloordiv__(self, other):
        """
        :types: other: int
        x.__rfloordiv__(y) <==> y//x
        """
        if self != 0:
            # Fix z3 bugs
            # 1) -1 == -1//2 != 1//-2  == 0
            # 2)  1 ==  1//2 != -1//-2 == 0
            if self < 0:
                return -other//-self
            else:
                return IntProxy(smt.Div(other.real, self.real))
        else:
            raise ZeroDivisionError("IntProxy division or modulo by zero")

    @check_self_and_other_have_same_type
    def __rmod__(self, other):
        """
        :types: other: int
        x.__rmod__(y) <==> y%x
        """
        if self != 0:
            return IntProxy(smt.Mod(other.real, self.real))
        else:
            raise ZeroDivisionError("IntProxy division or modulo by zero")

    @check_self_and_other_have_same_type
    def __rmul__(self, other):
        """
        :types: other: int
        x.__rmul__(y) <==> y*x
        """
        return self.__mul__(other)

    @check_self_and_other_have_same_type
    def __rsub__(self, other):
        """
        :types: other: int
        x.__rsub__(y) <==> y-x
        """
        return IntProxy(smt.Sub(other.real, self.real))

    @forward_to_rfun()
    def __sub__(self, other):
        """
        :types: other: int
        x.__sub__(y) <==> x-y
        """
        return IntProxy(smt.Sub(self.real, other.real))

    @forward_to_rfun()
    def __truediv__(self, other):
        """
        :types: other: int
        x.__truediv__(y) <==> x/y
        """
        raise NotImplementedError("FloatProxy not implemented.")

    def _concretize(self, model):
        if isinstance(self.real, int):
            result = self.real
        else:
            result = SMTInt.concreteValue(model.evaluate(self.real))
        return result


class BoolProxy(ProxyObject):
    """
    Bool Proxy object for variables that behave like bools.
    """
    emulated_class = bool

    def __init__(self, formula=None):
        if formula is None:
            formula = smt.Const(SMTBool)
        self.formula = formula

    def __not__(self):
        return BoolProxy(smt.Not(self.formula))

    def __bool__(self):
        if isinstance(self.formula, bool):
            return self.formula
        # If self.formula isn't a builtin bool we have to solve it
        partial_solve = self._get_partial_solve()
        if partial_solve != None:
            return partial_solve
        if len(self._path) > len(self._pathcondition):
            branch = self._path[len(self._pathcondition)]
            if branch:
                formula = self.formula
            else:
                formula = smt.Not(self.formula)
            self._pathcondition.append(formula)
            return branch
        if len(self._path) >= ProxyObject.max_depth:
            # self._path.pop()  # We go back to the previus branch
            raise MaxDepthError()
        self._path.append(True)
        self._pathcondition.append(self.formula)
        return True

    def __nonzero__(self):
        return self.__bool__()

    def _get_partial_solve(self):
        """
        :returns: None if constrains haven't define a concrete value yet,
                  else returns that concrete value (True or False).
        It tries to obtain a bool value if possible. Without branching.
        """
        conditions = True
        for c in self._pathcondition + self._facts:
            conditions = smt.And(conditions, c)
        true_cond  = smt.check(smt.And(conditions, self.formula))
        false_cond = smt.check(smt.And(conditions, smt.Not(self.formula)))
        # TODO: meter checkeo de indecibilidad acá
        if true_cond == "sat" and not false_cond == "sat":
            return True
        if false_cond == "sat" and not true_cond == "sat":
            return False

    def __repr__(self):
        ps = self._get_partial_solve()
        return "BoolProxy(%s)" % (ps if ps != None else str(self.formula))

    def __deepcopy__(self, memo):
        return self

    def _concretize(self, model):
        if isinstance(self.formula, bool):
            result = self.formula
        else:
            result = SMTBool.concreteValue((model.evaluate(self.formula)))
        return result


class ListProxy(ProxyObject):
    """
    List Proxy object for variables that behave like int lists.
    also, isinstance(ListProxy(), Iterable) == True
    """
    emulated_class = list

    def __init__(self, initial=None):
        self._array = smt.Const(SMTArray, SMTInt, SMTInt)
        self._start = 0  # For slice operations
        self._step = 1  # For slice operations
        if isinstance(initial, ListProxy):
            # Idempotent creation
            self._update_self(initial)
        elif isinstance(initial, Iterable):
            self._len = len(initial)
            for i, x in enumerate(initial):
                if not (isinstance(x, int) or isinstance(x, IntProxy)):
                    raise AssertionError("ListProxy can only contain numbers, "+
                            "%s containing %s of type %s given in __init__" % \
                            (str(initial), str(initial[0]), type(initial[0])))
                # Only support for list of ints or IntProxys for now.
                # FIXME: Extend it for at least list of list.
                if isinstance(x, IntProxy):
                    x = x.real
                self._array = smt.Store(self._array, i, x)
        else:
            self._len = IntProxy()
            boolproxy = self._len >= 0
            self._facts.append(boolproxy.formula)

    @forward_to_rfun()
    def __add__(self, other):
        """
        :types: other: list
        x.__add__(y) <==> x+y
        """
        return ListProxy([x for x in self] + [x for x in other])

    def __bool__(self):
        """
        x.__bool__() <==> len(self) > 0
        """
        # return self._len != 0
        return bool(self._len > 0)

    def __contains__(self, other):
        """
        :types: other: int
        x.__contains__(y) <==> y in x
        """
        for i in self:
            if i == other:
                return True
        return False

    def __delitem__(self, other):
        """
        :types: other: int
        x.__delitem__(y) <==> del x[y]
        """
        self.pop(other)
        # Deleting a item in Z3 gets messy, so we just pop the item.

    def __repr__(self):
        """
        x.__repr__() <==> repr(x)
        """
        if not isinstance(self._len, int):
            s = "[ ... | length = %s]" % self._len
        else:
            l = ["%s" % x for x in self]
            s = "[%s]" % " ,".join(l)
        return s

    def __eq__(self, other):
        """
        :types: other: list
        x.__eq__(y) <==> x == y
        """
        result = False
        if len(self) == len(other):
            result = True
            for i in range(len(self)):
                if self[i] != other[i]:
                    result = False
                    break
        return result

    def __ge__(self, other):
        """
        :types: other: list
        x.__ge__(y) <==> x>=y
        """
        # Equivalent to: return self > other or self == other
        if not self and other:  # [] < [, ] always
            return False
        min_l = min(len(self), len(other))
        for i in range(min_l):
            if self[i] < other[i]:
                return False
            if self[i] > other[i]:
                return True
        return len(self) >= len(other)

    def __getitem__(self, i):
        """
        :types: i:[int, slice]
        x.__getitem__(y) <==> x[y]
        """
        if isinstance(i, int) or isinstance(i, IntProxy):
            index = self._get_index(i)
            if index is None:
                raise IndexError("ListProxy index out of range")
            result = IntProxy(smt.Select(self._array, index.real))
        elif isinstance(i, slice) or isinstance(i, SliceProxy):
            # Transform the slice to a SliceProxy
            i = SliceProxy(i)
            # Calculate the real start/stop/step
            start, stop, step = i.indices(self._len)
            start_i = self._get_index(start)
            if start_i is None:
                # An out of range start means the result list will be []
                start, stop, step = 0, 0, 1
                start_i = self._start
            result = copy.deepcopy(self)
            result._start = start_i
            result._step = self._step * step
            # Watch out for negative lengths!
            fix = 0 if (stop - start) % step == 0 else 1
            result._len = max(0, (stop - start) // step + fix)
            return result
        else:
            raise TypeError("ListProxy indices must be integers, not %s" % type(i).__name__)
        return result

    def __gt__(self, other):
        """
        :types: other: list
        x.__ge__(y) <==> x>y
        """
        if not self:  # [] < [, ] always
            return False
        min_l = min(len(self), len(other))
        for i in range(min_l):
            if self[i] < other[i]:
                return False
            if self[i] > other[i]:
                return True
        return len(self) > len(other)

    def __iadd__(self, other):
        """
        :types: other: list
        x.__iadd__(y) <==> x+=y
        """
        self.extend(other)
        return self

    def __imul__(self, n):
        """
        :types: n: int
        x.__imul__(y) <==> x*=y
        """
        if n <= 0:
            self.clear()
        elif n > 1:
            #TODO: se puede sin branching? (hace falta una copia inmutable)
            self_copy = ListProxy([x for x in self])
            for i in range(n - 1):
                self += self_copy
        return self

    def __iter__(self):
        """
        x.__iter__() <==> iter(x)
        """
        # FIXME: return list-iterator object instead of generator to match real
        # lists behaviour
        # All the start/stop/step logic is solved in __getattr__ (get_index)
        i = 0
        while i < self.__len__():
            yield self[i]
            i += 1

    def __le__(self, other):
        """
        :types: other: list
        x.__ge__(y) <==> x<y
        """
        return not self > other

    def __len__(self):
        """
        x.__len__() <==> len(x)
        """
        return self._len

    def __lt__(self, other):
        """
        :types: other: list
        x.__ge__(y) <==> x<y
        """
        return not self >= other

    @forward_to_rfun(int, IntProxy)
    def __mul__(self, n):
        """
        :types: n: int
        x.__mul__(y) <==> x*y
        """
        result = self.copy()
        result *= n
        return result

    def __rmul__(self, n):
        """
        :types: n: int
        x.__rmul__(y) <==> y*x
        """
        return self * n

    def __setitem__(self, i, y):
        """
        :types: i:[int, slice],  y:[int, ]
        x.__setitem__(i, y) <==> x[i]=y
        """
        if isinstance(i, int) or isinstance(i, IntProxy):
            index = self._get_index(i)
            if index is None:
                raise IndexError("ListProxy assignment index out of range.")
            self._array = smt.Store(self._array, index.real, y.real)
        elif isinstance(i, slice) or isinstance(i, SliceProxy):
            # Transform the slice to a SliceProxy
            i = SliceProxy(i)
            # Calculate the real start/stop/step
            start, stop, step = i.indices(self._len)
            # If start is out of range, set default values
            if not 0 <= start < self._len:
                start, stop, step = 0, 0, 1
            # Watch out with negative lengths!
            fix = 0 if (stop - start) % step == 0 else 1
            length = max(0, (stop - start) // step + fix)
            # Get `length` elements from y
            iterable_y = iter(y)
            to_assign = []
            try:
                [to_assign.append(next(iterable_y)) for x in range(length)]
            except StopIteration:
                raise ValueError("attempt to assign sequence of size %s to "\
                     "extended slice of size %s" % (len(to_assign), length))
            for i in range(start, stop, step):
                self[i] = to_assign[i]
        else:
            raise TypeError("ListProxy indices must be integers, not %s" % type(i).__name__)

    def append(self, x):
        """
        :types: x: int
        L.append(object) -> None -- append object to end
        """
        index = self.__len__()
        self._len += 1
        self[index] = x

    def clear(self):
        """
        L.clear() -> None -- remove all items from L
        """
        self._len = 0
        self._start = 0
        self._step = 0

    def copy(self):
        """
        L.copy() -> list -- a shallow copy of L
        """
        return ListProxy(self)

    def count(self, value):
        """
        :types: value: int
        L.count(value) -> integer -- return number of occurrences of value
        """
        r = 0
        for x in self:
            if x == value:
                r += 1
        return r

    def extend(self, iterable):
        """
        :types: iterable: list
        L.extend(iterable) -> None -- extend list by appending elements from the iterable
        """
        for x in iterable:
            self.append(x)

    @handle_varargs_and_defaults(0, None)
    def index(self, value, *args):
        """
        :types: value: int
        L.index(value, [start, [stop]]) -> integer -- return first index of value.
        :raises: ValueError if the value is not present.
        """
        start, stop = args[0], args[1] if args[1] != None else len(self)
        result = None
        try:
            start = self._get_positive_index(start)
        except IndexError:
            raise ValueError("%s not in ListProxy" % (value))
        for i, x in enumerate(self[start:stop], start):
            if x == value:
                result = i
                break
        if result == None:
            raise ValueError("%s is not in ProxyList" % (value))
        else:
            return result

    def insert(self, index, obj):
        """
        :types: index: int, obj: [int, ]
        L.insert(index, object) -> None -- insert object before index
        """
        prefix = self[:index]
        postfix = self[index:]
        self._update_self(ListProxy(prefix + [obj] + postfix))

    @handle_varargs_and_defaults(-1)
    def pop(self, *args):
        """
        L.pop([index]) -> item -- remove and return item at index (default last).
        :raises: IndexError if list is empty or index is out of range.
        """
        i = self._get_positive_index(args[0])
        result = self[i]
        new_self = ListProxy([x for i, x in enumerate(self) if i != i])
        self._array = new_self._array
        self._len = self._len - 1  # We do this to keep _len symbolic
        return result

    def remove(self, value):
        """
        :types: value: [int,]
        L.remove(value) -> None -- remove first occurrence of value.
        :raises: ValueError if value not in self
        """
        i = self.index(value)
        self.pop(i)

    def reverse(self):
        """
        L.reverse() -- reverse *IN PLACE*
        """
        # Change direction
        self._step *= -1
        # Now we start with the last element
        self._start = self._get_index(-1)

    def sort(self, key=None, reverse=False):
        """
        L.sort(key=None, reverse=False) -> None -- stable sort *IN PLACE*
        """
        result = self._sort(key, reverse)
        if reverse:
            result.reverse()
        self._update_self(ListProxy(result))

    def _sort(self, key=None, reverse=False):
        """
        :types: reverse: bool
        L.sort(key=None, reverse=False) -> None
        """
        if not self:
            return []
        else:
            if key == None:
                key = lambda x: x
            pivot = self[0]
            lesser = ListProxy([])
            for x in self[1:]:
                if key(x) < key(pivot):
                    lesser.append(x)
            lesser = lesser._sort()
            greater = ListProxy([])
            for x in self[1:]:
                if key(x) >= key(pivot):
                    greater.append(x)
            greater = greater._sort()
            # lesser = self._sort([x for x in self[1:] if key(x) < key(pivot)], key)
            # greater = self._sort([x for x in self[1:] if key(x) >= key(pivot)], key)
            result = lesser + [pivot] + greater
            return result

    def _get_positive_index(self, i):
        """
        Given an index `i` representing a position inside a list,
        _get_positive_index returns a positive index that is relative to the
        given `i' position in the array.
        WARNING: To find the corresponding index in the Z3 array `_array', use
        _get_index instead.
        :raises: IndexError if i out of range of self
        """
        if 0 <= i < len(self):
            result = IntProxy(i)  # Positive index in valid range
        elif -len(self) <= i < 0:
            result = IntProxy(len(self) + i)  # Negative index in valid range
        else:
            raise IndexError("Index out of range")
        return result

    def _get_index(self, i):
        """
        Given an index `i` representing a position inside a list, _get_index
        deals with the start/length/step/`negative index` logic and returns
        the corresponding index for the private attribute _array or None if out
        of range.
        WARNING: Use this to get position in the Z3 array _array.
        To calculate positive relative index, use _get_positive_index instead.
        """
        try:
            positive_index = self._get_positive_index(i)
        except IndexError:
            return None
        return self._start + self._step * positive_index

    def __deepcopy__(self, memo):
        return copy.copy(self)

    def _update_self(self, other):
        """
        copy `other` attributes into `self`.
        """
        self.__dict__ = other.__dict__

    def _concretize(self, model):
        self._len = _concretize(self._len, model)  # First we concretize the length
        if isinstance(self._step, IntProxy):
            self._step = self._step._concretize(model)
        self._start = _concretize(self._start, model)
        return [x._concretize(model) for x in self]  # Then all comes naturally...


class StringProxy(ProxyObject):
    emulated_class = str

    def __init__(self, initial=None):
        if isinstance(initial, str):
            # Camouflage
            array = smt.Const(SMTArray, SMTInt, SMTChar)
            start, step, length = 0, 1, len(initial)
            for i, e in enumerate(initial):
                array = smt.Store(array, i, ord(e))
        elif isinstance(initial, StringProxy):
            # Creator idempotence
            array = initial._array
            start, step, length = initial._start, initial._step, initial._len
        elif SMTChar.belongsToSort(initial):
            # Special case, a char is also a string
            array = smt.Store(smt.Const(SMTArray, SMTInt, SMTChar), 0, initial)
            start, step, length = 0, 1, 1
        elif initial is None:
            array = smt.Const(SMTArray, SMTInt, SMTChar)
            start, step, length = 0, 1, IntProxy()
            boolproxy = length >= 0
            self._facts.append(boolproxy.formula)
        else:
            raise TypeError("type for initial: (%s) not supported" %\
                            (str(type(initial).__name__)))
        self._array = array
        self._start = start
        self._step = step
        self._len = length

    @forward_to_rfun()
    def __add__(self, other):
        """
        :type: other: str
        x.__add__(y) <==> x+y
        """
        result = StringProxy(self)
        for x in other:
            result = result._append(x)
        return result

    def __bool__(self):
        """
        x.__bool__() <==> len(self) > 0
        """
        # return self._len != 0
        return bool(self._len > 0)

    def __contains__(self, elem):
        """
        :type: elem: str
        x.__contains__(y) <==> y in x
        """
        return self.find(elem) != -1

    def __copy__(self):
        return StringProxy(self)

    def __deepcopy__(self, memo):
        return StringProxy(self)

    @check_equality
    def __eq__(self, other):
        """
        :type: other: str
        x.__eq__(y) <==> x==y
        """
        result = False
        if len(self) == len(other):
            result = True
            for i in range(len(self)):
                self_i = self[i]._array[0]
                if isinstance(other, str):
                    other_i = ord(other[i])
                else:
                    other_i = other[i]._array[0]
                if BoolProxy(smt.Eq(self_i, other_i)):
                    continue
                else:
                    result = False
                    break
        return result

    def __format__(self, *args, **kwargs):
        """
        S.__format__(format_spec) -> str

        Return a formatted version of S as described by format_spec.
        """
        if len(args) == 1 and not args[0]:
            return str(self)  # Base case
        else:
            raise NotImplementedError()

    @check_comparable_types
    def __ge__(self, other):
        """
        :type: other: str
        x.__ge__(y) <==> x>=y
        """
        # Equivalent to: return self > other or self == other
        if not self and other:  # [] < [, ] always
            return False
        min_l = min(len(self), len(other))
        for i in range(min_l):
            self_i = self[i]._array[0]
            if isinstance(other, str):
                other_i = ord(other[i])
            else:
                other_i = other[i]._array[0]
            if BoolProxy(smt.Lt(self_i, other_i)):
                return False
            if BoolProxy(smt.Gt(self_i, other_i)):
                return True
        return len(self) >= len(other)

    def __getitem__(self, i):
        """
        :types: i:[int, slice]
        x.__getitem__(y) <==> x[y]
        """
        if isinstance(i, int) or isinstance(i, IntProxy):
            index = self._get_index(i)
            if index is None:
                raise IndexError("StringProxy index out of range")
            result = StringProxy(smt.Select(self._array, index.real))
        elif isinstance(i, slice) or isinstance(i, SliceProxy):
            # Transform the slice to a SliceProxy
            i = SliceProxy(i)
            # Calculate the real start/stop/step
            start, stop, step = i.indices(self._len)
            start_i = self._get_index(start)
            if start_i is None:
                # An out of range start means the result list will be []
                start, stop, step = 0, 0, 1
                start_i = self._start
            result = copy.deepcopy(self)
            result._start = start_i
            result._step = self._step * step
            # Watch out for negative lengths!
            fix = 0 if (stop - start) % step == 0 else 1
            result._len = max(0, (stop - start) // step + fix)
            return result
        else:
            raise TypeError("ListProxy indices must be integers, not %s" % type(i).__name__)
        return result

    @check_comparable_types
    def __gt__(self, other):
        """
        :type: other: str
        x.__gt__(y) <==> x>y
        """
        if not self:  # [] < [, ] always
            return False
        min_l = min(len(self), len(other))
        for i in range(min_l):
            self_i = self[i]._array[0]
            if isinstance(other, str):
                other_i = ord(other[i])
            else:
                other_i = other[i]._array[0]
            if BoolProxy(smt.Lt(self_i, other_i)):
                return False
            if BoolProxy(smt.Gt(self_i, other_i)):
                return True
        return len(self) > len(other)

    def __hash__(self, *args, **kwargs):
        """
        x.__hash__() <==> hash(x)
        """
        raise NotImplementedError()

    def __iter__(self):
        """
        x.__iter__() <==> iter(x)
        """
        # All the start/stop/step logic is solved in __getattr__ (get_index)
        i = 0
        while i < self.__len__():
            yield self[i]
            i += 1

    def __le__(self, other):
        """
        :type: other: str
        x.__le__(y) <==> x<=y
        """
        return not self > other

    def __len__(self):
        """
        x.__len__() <==> len(x)
        """
        return self._len

    def __lt__(self, other):
        """
        :types: other: str
        x.__ge__(y) <==> x<y
        """
        return not self >= other

    def __mod__(self, *args, **kwargs):
        """
        x.__mod__(y) <==> x%y
        """
        raise NotImplementedError()

    @forward_to_rfun(int, IntProxy)
    def __mul__(self, times):
        """
        :type: times: int
        x.__mul__(n) <==> x*n
        """
        result = StringProxy("")
        times = max(0, times)
        for x in range(times):
            result += self
        return result

    def __ne__(self, other):
        """
        :type: other: str
        x.__ne__(y) <==> x!=y
        """
        return not (self == other)

    @check_self_and_other_have_same_type
    def __radd__(self, other):
        """
        :types: other: str
        x.__radd__(y) <==> y+x
        """
        return self.__add__(other)

    def __repr__(self):
        """
        x.__repr__() <==> repr(x)
        """
        if not isinstance(self._len, int):
            return "'...' | length = %s" % self._len
        # If we know the len, we can return something more representative
        s = ""
        for x in self:
            s_x = smt.simplify(x._array[0])
            if SMTChar.isSMTValue(s_x):
                s += chr(SMTChar.concreteValue(s_x))
            else:
                s += "▢"
        s = "'%s'" % s
        return s

    def __rmod__(self, *args, **kwargs):
        """
        x.__rmod__(y) <==> y%x
        """
        raise NotImplementedError()

    def __rmul__(self, times):
        """
        :type: times: int
        x.__rmul__(n) <==> n*x
        """
        return self.__mul__(times)

    def __str__(self):
        """
        x.__str__() <==> str(x)
        """
        return self.__repr__()

    def _append(self, elem):
        """
        Given a character `elem`, _append returns a new string equivalent to
        self + elem
        """
        result = StringProxy(self)
        to_append = ord(elem) if isinstance(elem, str) else elem._array[0]
        result._len += 1
        index = result._get_index(-1).real
        result._array = smt.Store(self._array, index, to_append)
        return result

    def _concretize(self, model):
        self._len = _concretize(self._len, model)  # First we concretize the length
        self._step = _concretize(self._step, model)
        self._start = _concretize(self._start, model)
        result = "".join([x._concretize_char(model) for x in self])
        return result

    def _concretize_char(self, model):
        char = model.evaluate(self._array[0])
        return chr(SMTChar.concreteValue(char))

    def _get_index(self, i):
        """
        Given an index `i` representing a position inside a list, _get_index
        deals with the start/length/step/`negative index` logic and returns
        the corresponding index for the private attribute _array.
        """
        index = None
        if 0 <= i < self.__len__():
            index = IntProxy(i)  # Positive index in valid range
        elif -self.__len__() <= i < 0:
            index = IntProxy(self.__len__() + i)  # Negative index in valid range
        # If index is something compute the start/step offset
        return self._start + self._step * index if index != None else index

    def _prepend(self, elem):
        """
        Given a character `elem`, _prepend returns a new string equivalent to
        elem + self
        """
        result = StringProxy(self)
        to_prepend = ord(elem) if isinstance(elem, str) else elem._array[0]
        result._len += 1
        result._start = self._start - self._step
        result._array = smt.Store(self._array, result._start.real, to_prepend)
        return result

    def capitalize(self):
        """
        S.capitalize() -> str

        Return a capitalized version of S, i.e. make the first character
        have upper case and the rest lower case.
        """
        return self.upper(self[0]) + self[1:] if self else self

    def casefold(self):
        """
        S.casefold() -> str

        Return a version of S suitable for caseless comparisons.
        """
        # TODO: https://mail.python.org/pipermail/python-ideas/2012-January/013293.html
        # casefold it is not just a call to self.lower
        raise NotImplementedError("StringProxy.casefold")

    @handle_varargs_and_defaults(' ')
    def center(self, width, *args):
        """
        :types: width: int, args: str

        S.center(width[, fillchar]) -> str

        Return S centered in a string of length width. Padding is
        done using the specified fill character (default is a space)
        """
        fillchar = StringProxy(args[0])
        if width < len(self):
            return self
        else:
            marg = width - len(self);
            # Algorithm copied from:
            #http://svn.python.org/view/python/trunk/Objects/stringobject.c
            pad_l = marg // 2 + (1 if width % 2 == 1 and len(self) % 2 == 0 else 0)
            pad_r = marg - pad_l
            return pad_l * fillchar + self + pad_r * fillchar

    @handle_varargs_and_defaults(None, None)
    def count(self, sub, *args):
        """
        :types: sub: str

        S.count(sub[, start[, end]]) -> int

        Return the number of non-overlapping occurrences of substring sub in
        string S[start:end].  Optional arguments start and end are
        interpreted as in slice notation.
        """
        start, stop = args
        start, stop = 0 if start is None else start, len(self) if stop is None else stop
        c = 0
        while True:
            try:
                # Non-overlaping ocurrences
                start = self.index(sub, start, stop) + max(len(sub), 1)
                c += 1
            except ValueError:
                break
        return c

    def encode(self, encoding='uft-8', errors='strict'):
        """
        S.encode(encoding='utf-8', errors='strict') -> bytes

        Encode S using the codec registered for encoding. Default encoding
        is 'utf-8'. errors may be given to set a different error
        handling scheme. Default is 'strict' meaning that encoding errors raise
        a UnicodeEncodeError. Other possible values are 'ignore', 'replace' and
        'xmlcharrefreplace' as well as any other name registered with
        codecs.register_error that can handle UnicodeEncodeErrors.
        """
        raise NotImplementedError("StringProxy.encode")

    @handle_varargs_and_defaults(None, None)
    def endswith(self, suffix, *args):
        """
        :types: suffix: str, args: list

        S.endswith(suffix[, start[, end]]) -> bool

        Return True if S ends with the specified suffix, False otherwise.
        With optional start, test S beginning at that position.
        With optional end, stop comparing S at that position.
        suffix can also be a tuple of strings to try.
        """
        start, stop = args
        start, stop = 0 if start is None else start, len(self) if stop is None else stop
        sliced_string = self[start:stop]
        return sliced_string[len(sliced_string) - len(suffix):] == suffix

    @handle_varargs_and_defaults(8)
    def expandtabs(self, *args):
        """
        :types: args: list

        S.expandtabs([tabsize]) -> str

        Return a copy of S where all tab characters are expanded using spaces.
        If tabsize is not given, a tab size of 8 characters is assumed.
        """
        tabsize = args[0]
        return self.replace('\t', tabsize * StringProxy(' '))

    @handle_varargs_and_defaults(None, None)
    def find(self, sub, *args):
        """
        :types: sub: str, args: list

        S.find(sub[, start[, end]]) -> int

        Return the lowest index in S where substring sub is found,
        such that sub is contained within S[start:end].  Optional
        arguments start and end are interpreted as in slice notation.

        Return -1 on failure.
        """
        start, stop = args
        start, stop = 0 if start is None else start, len(self) if stop is None else stop
        start = max(start + len(self), 0) if start < 0 else start
        if stop < 0:
            stop = max(stop + len(self), 0)
        else:
            stop = min(stop, len(self))
        if start > len(self):
            return -1
        elif stop < start:
            return -1
        elif sub == '':
            return start
        else:
            for i in range(start, stop - len(sub) + 1):
                if self[i:i + len(sub)] == sub:
                    return i
        return -1

    def format(self, *args, **kwargs):
        """
        S.format(*args, **kwargs) -> str

        Return a formatted version of S, using substitutions from args and kwargs.
        The substitutions are identified by braces ('{' and '}').
        """
        raise NotImplementedError("StringProxy.format")

    def format_map(self, mapping):
        """
        S.format_map(mapping) -> str

        Return a formatted version of S, using substitutions from mapping.
        The substitutions are identified by braces ('{' and '}').
        """
        raise NotImplementedError()

    def index(self, sub, *args):
        """
        :types: sub: str, args: list

        S.index(sub[, start[, end]]) -> int

        Like S.find() but raise ValueError when the substring is not found.
        """
        i = self.find(sub, *args)
        if i == -1:
            raise ValueError("substring not found")
        else:
            return i

    def isalnum(self):
        """
        S.isalnum() -> bool

        Return True if all characters in S are alphanumeric
        and there is at least one character in S, False otherwise.
        """
        raise NotImplementedError()

    def isalpha(self):
        """
        S.isalpha() -> bool

        Return True if all characters in S are alphabetic
        and there is at least one character in S, False otherwise.
        """
        raise NotImplementedError()

    def isdecimal(self):
        """
        S.isdecimal() -> bool

        Return True if there are only decimal characters in S,
        False otherwise.
        """
        # TODO: This is not the right implementation.
        return bool(self) and not [x for x in self if x not in "0123456789".split()]

    def isdigit(self):
        # TODO: This is not the right implementation.
        """
        S.isdigit() -> bool

        Return True if all characters in S are digits
        and there is at least one character in S, False otherwise.
        """
        return bool(self) and not [x for x in self if x not in "0123456789".split()]

    def isidentifier(self):
        """
        S.isidentifier() -> bool

        Return True if S is a valid identifier according
        to the language definition.

        Use keyword.iskeyword() to test for reserved identifiers
        such as "def" and "class".
        """
        raise NotImplementedError()

    def islower(self):
        """
        S.islower() -> bool

        Return True if all cased characters in S are lowercase and there is
        at least one cased character in S, False otherwise.
        """
        raise NotImplementedError()

    def isnumeric(self):
        """
        S.isnumeric() -> bool

        Return True if there are only numeric characters in S,
        False otherwise.
        """
        raise NotImplementedError()

    def isprintable(self):
        """
        S.isprintable() -> bool

        Return True if all characters in S are considered
        printable in repr() or S is empty, False otherwise.
        """
        raise NotImplementedError()

    def isspace(self):
        """
        S.isspace() -> bool

        Return True if all characters in S are whitespace
        and there is at least one character in S, False otherwise.
        """
        return bool(self) and not [x for x in self if x not in StringProxy(" \t\r\n")]

    def istitle(self):
        """
        S.istitle() -> bool

        Return True if S is a titlecased string and there is at least one
        character in S, i.e. upper- and titlecase characters may only
        follow uncased characters and lowercase characters only cased ones.
        Return False otherwise.
        """
        result = len(self) > 1
        last_one_was_space = True
        for i in range(len(self)):
            if last_one_was_space and self[i].islower():
                result = False
                break
            elif not last_one_was_space and self[i].isupper():
                result = False
                break
            last_one_was_space = self[i].isspace()
        return result

    def isupper(self):
        """
        S.isupper() -> bool

        Return True if all cased characters in S are uppercase and there is
        at least one cased character in S, False otherwise.
        """
        raise NotImplementedError()

    # TODO: Change the type contract to join strings and not only chars
    def join(self, iterable):
        """
        :types: iterable: str

        S.join(iterable) -> str

        Return a string which is the concatenation of the strings in the
        iterable.  The separator between elements is S.
        """
        result = ""
        first_loop = True
        iterator = iter(iterable)
        try:
            while True:
                next_string = next(iterator)
                if not first_loop:
                    result += self
                else:
                    first_loop = False
                result += next_string
        except StopIteration:
            pass
        return result

    @handle_varargs_and_defaults(' ')
    def ljust(self, width, *args):
        """
        :types: width: int, args: str

        S.ljust(width[, fillchar]) -> str

        Return S left-justified in a Unicode string of length width. Padding is
        done using the specified fill character (default is a space).
        """
        fillchar = StringProxy(args[0])
        return self + max(0, width - len(self)) * fillchar

    def lower(self, *args, **kwargs):
        """
        S.lower() -> str

        Return a copy of the string S converted to lowercase.
        """
        raise NotImplementedError()

    # TODO: Change typecontracts behavior! This only test with chars
    @handle_varargs_and_defaults(None)
    def lstrip(self, *args):
        """
        :types: args: str

        S.lstrip([chars]) -> str

        Return a copy of the string S with leading whitespace removed.
        If chars is given and not None, remove characters in chars instead.
        """
        remove = lambda x: x in StringProxy(args[0]) if args[0] else x.isspace()
        i = 0
        while i < len(self):
            x = self[i]
            if not remove(x):
                break
            i += 1
        return self[i:]

    def maketrans(self, *args, **kwargs):
        """
        str.maketrans(x[, y[, z]]) -> dict (static method)

        Return a translation table usable for str.translate().
        If there is only one argument, it must be a dictionary mapping Unicode
        ordinals (integers) or characters to Unicode ordinals, strings or None.
        Character keys will be then converted to ordinals.
        If there are two arguments, they must be strings of equal length, and
        in the resulting dictionary, each character in x will be mapped to the
        character at the same position in y. If there is a third argument, it
        must be a string, whose characters will be mapped to None in the result.
        """
        raise NotImplementedError()

    def partition(self, sep):
        """
        S.partition(sep) -> (head, sep, tail)

        Search for the separator sep in S, and return the part before it,
        the separator itself, and the part after it.  If the separator is not
        found, return S and two empty strings.
        """
        if len(sep) == 0:
            raise ValueError("empty separator")
        i = self.find(sep)
        head = self[:i]
        if i == -1:
            sep = self[i:i]
            tail = self[i:i]
        else:
            tail = self[i + len(sep):]
        return head, sep, tail

    @handle_varargs_and_defaults(None)
    def replace(self, old, new, *args):
        """
        :types: old: str, new: str, args: list

        S.replace(old, new[, count]) -> str

        Return a copy of S with all occurrences of substring
        old replaced by new.  If the optional argument count is
        given, only the first count occurrences are replaced.
        """
        start, count = 0, args[0] if args[0] != None else len(self)
        result, c = StringProxy(""), 0
        try:
            while c < count:
                index = self.index(old, start)
                result += self[start:index] + new
                start = index + max(len(old), 1)
                c += 1
        except ValueError:
            result += self[start:]
        return result

    @handle_varargs_and_defaults(None, None)
    def rfind(self, sub, *args):
        """
        :types: sub: str, args: list

        S.rfind(sub[, start[, end]]) -> int

        Return the highest index in S where substring sub is found,
        such that sub is contained within S[start:end].  Optional
        arguments start and end are interpreted as in slice notation.

        Return -1 on failure.
        """
        start, stop = args
        start, stop = 0 if start is None else start, len(self) if stop is None else stop
        start = max(start + len(self), 0) if start < 0 else start
        if stop < 0:
            stop = max(stop + len(self), 0)
        else:
            stop = min(stop, len(self))
        if start > len(self):
            return -1
        elif stop < start:
            return -1
        elif sub == '':
            return stop
        else:
            for i in range(stop - len(sub), start - 1, -1):
                if self[i:i + len(sub)] == sub:
                    return i
        return -1

    def rindex(self, sub, *args):
        """
        :types: sub: str, args: list

        S.rindex(sub[, start[, end]]) -> int

        Like S.rfind() but raise ValueError when the substring is not found.
        """
        i = self.rfind(sub, *args)
        if i == -1:
            raise ValueError("substring not found")
        else:
            return i

    def rjust(self, width, *args):
        """
        :types: width: int, args: str

        S.rjust(width[, fillchar]) -> str

        Return S right-justified in a string of length width. Padding is
        done using the specified fill character (default is a space).
        """
        return self[::-1].ljust(width, *args)[::-1]

    def rpartition(self, sep):
        """
        S.rpartition(sep) -> (head, sep, tail)

        Search for the separator sep in S, starting at the end of S, and return
        the part before it, the separator itself, and the part after it.  If the
        separator is not found, return two empty strings and S.
        """
        head, sep, tail = self[::-1].partition(sep[::-1])
        return tail[::-1], sep[::-1], head[::-1]

    def rsplit(self, sep=None, maxsplit=-1):
        """
        S.rsplit(sep=None, maxsplit=-1) -> list of strings

        Return a list of the words in S, using sep as the
        delimiter string, starting at the end of the string and
        working to the front.  If maxsplit is given, at most maxsplit
        splits are done. If sep is not specified, any whitespace string
        is a separator.
        """
        issep = (lambda x: x in sep) if sep != None else (lambda x: x.isspace())
        maxsplit = maxsplit if maxsplit >= 0 else len(self) + 1
        i, splits = len(self) - 1, 0
        result = []
        acum = ""
        while i >= 0:
            if issep(self[i]) and splits < maxsplit:
                result.insert(0, acum)
                splits += 1
                acum = ""
            else:
                acum = self[i] + acum
            i -= 1
        result.insert(0, acum)
        return result

    def rstrip(self, *args):
        """
        S.rstrip([chars]) -> str

        Return a copy of the string S with trailing whitespace removed.
        If chars is given and not None, remove characters in chars instead.
        """
        return self[::-1].strip(*args)[::-1]

    def split(self, sep=None, maxsplit=-1):
        """
        S.split(sep=None, maxsplit=-1) -> list of strings

        Return a list of the words in S, using sep as the
        delimiter string.  If maxsplit is given, at most maxsplit
        splits are done. If sep is not specified or is None, any
        whitespace string is a separator and empty strings are
        removed from the result.
        """
        issep = (lambda x: x in sep) if sep != None else (lambda x: x.isspace())
        maxsplit = maxsplit if maxsplit >= 0 else len(self) + 1
        i, splits = 0, 0
        result = []
        acum = ""
        while i < len(self):
            if issep(self[i]) and splits < maxsplit:
                result.insert(len(self), acum)
                splits += 1
                acum = ""
            else:
                acum += self[i]
            i += 1
        result.insert(len(self), acum)
        return result

    @handle_varargs_and_defaults(False)
    def splitlines(self, *args):
        """
        :types: args: list
        S.splitlines([keepends]) -> list of strings

        Return a list of the lines in S, breaking at line boundaries.
        Line breaks are not included in the resulting list unless keepends
        is given and true.
        """
        keepends = args[0]
        i = 0
        result = []
        acum = ""
        while i < len(self):
            if self[i] == '\r' or self[i] == '\n':
                end = self[i]
                if i < len(self) - 1 and self[i:i+2] == '\r\n':
                    end = '\r\n'
                    i += 1
                if keepends:
                    acum += end
                result.insert(len(self), acum)
                acum = ""
            else:
                acum += self[i]
            i += 1
        result.insert(len(self), acum)
        return result

    @handle_varargs_and_defaults(None, None)
    def startswith(self, prefix, *args):
        """
        S.startswith(prefix[, start[, end]]) -> bool

        Return True if S starts with the specified prefix, False otherwise.
        With optional start, test S beginning at that position.
        With optional end, stop comparing S at that position.
        prefix can also be a tuple of strings to try.
        """
        start, stop = args
        return self[start:stop] == prefix

    def strip(self, *args):
        """
        S.strip([chars]) -> str

        Return a copy of the string S with leading and trailing
        whitespace removed.
        If chars is given and not None, remove characters in chars instead.
        """
        return self.lstrip(*args).rstip(*args)

    def swapcase(self):
        """
        S.swapcase() -> str

        Return a copy of S with uppercase characters converted to lowercase
        and vice versa.
        """
        result = ""
        for c in self:
            if c.isupper():
                result += c.lower()
            elif x.islower():
                result += c.upper()
            else:
                result += c
        return result

    def title(self):
        """
        S.title() -> str

        Return a titlecased version of S, i.e. words start with title case
        characters, all remaining cased characters have lower case.
        """
        result = ""
        last_one_was_space = True
        for i in range(len(self)):
            if last_one_was_space and not self[i].isspace():
                result += self[i].upper()
            elif not last_one_was_space and not self[i].isspace():
                result += self[i].lower()
            else:
                result += self[i]
            last_one_was_space = self[i].isspace()
        return result

    def translate(self, *args, **kwargs):
        """
        S.translate(table) -> str

        Return a copy of the string S, where all characters have been mapped
        through the given translation table, which must be a mapping of
        Unicode ordinals to Unicode ordinals, strings, or None.
        Unmapped characters are left untouched. Characters mapped to None
        are deleted.
        """
        raise NotImplementedError()

    def upper(self, *args, **kwargs):
        """
        S.upper() -> str

        Return a copy of S converted to uppercase.
        """
        raise NotImplementedError()

    def zfill(self, *args, **kwargs):
        """
        S.zfill(width) -> str

        Pad a numeric string S with zeros on the left, to fill a field
        of the specified width. The string S is never truncated.
        """
        raise NotImplementedError()

class SliceProxy(ProxyObject):
    emulated_class = slice

    def __init__(self, initial=None):
        if isinstance(initial, slice) or isinstance(initial, SliceProxy):
            self.start = initial.start
            self.stop = initial.stop
            self.step = initial.step
        elif initial is None:
            nargs = IntProxy()
            self.start = None
            self.step = None
            if nargs == 1:
                self.stop = IntProxy()
            elif nargs == 2:
                self.start = IntProxy()
                self.stop = IntProxy()
            else:
                self.start = IntProxy()
                self.stop = IntProxy()
                self.step = IntProxy()
                self._facts.append((self.step != 0).formula)

        else:
            raise TypeError("type for initial: (%s) not supported" %\
                            (str(type(initial).__name__)))

    def indices(self, len):
        """
        :types: len: int

        Had to read Python slice.indices() source code to find out the right way.
        """
        start = self.start
        stop = self.stop
        step = self.step if self.step != None else 1
        if start is None:
            start = len-1 if step < 0 else 0
        start = start + len if start < 0 else start
        if start < 0:
            start = -1 if step < 0 else 0
        if start >= len:
            start = len -1 if step < 0 else len
        if stop is None:
            stop = -1 if step < 0 else len
        stop = stop + len if stop < 0 else stop
        if stop < 0:
            stop = -1 if step < 0 else 0
        if stop >= len:
            stop = len -1 if step < 0 else len
        if step == 0:
            raise ValueError("SliceProxy step cannot be zero")
        return (start, stop, step)

    def __repr__(self):
        return "SliceProxy(%s, %s, %s)" % (self.start, self.stop, self.step)

    def _concretize(self, model):
        start = _concretize(self.start, model)
        stop = _concretize(self.stop, model)
        step = _concretize(self.step, model)
        return slice(start, stop, step)
