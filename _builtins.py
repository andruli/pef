from builtins import __import__, __build_class__
from builtins import *
import builtins
import proxy
import sys
sys.modules[__name__].__setattr__("__debug__", builtins.__debug__)


def len(x):
    if builtins.isinstance(x, proxy.ProxyObject) or hasattr(x, "_concretize"):
        result = x.__len__()
        assert (isinstance(result, proxy.IntProxy) or isinstance(result, int))
    else:
        result = builtins.len(x)
    return result


def type(*args):
    result = None
    if len(args) != 1:
        result = builtins.type(*args)
    else:
        obj = args[0]
        if builtins.isinstance(obj, proxy.ProxyObject):
            result = obj.emulated_class  # We are such liars!
        else:
            result = builtins.type(obj)
    return result


def isinstance(*args):
    if len(args) != 2:
        raise TypeError("isinstance expected 2 arguments, got %s" %(len(args)))
    else:
        if builtins.isinstance(args[0], proxy.ProxyObject):
            args = (args[0].emulated_class(), args[1])
        return builtins.isinstance(*args)


def range(*args):
    """
    range(stop) -> range object
    range(start, stop[, step]) -> range object

    Return a virtual sequence of numbers from start to stop by step.
    """
    l = len(args)
    if l < 1:
        raise TypeError("range expected 1 arguments, got 0")
    elif l > 3:
        raise TypeError("range expected at most 3 arguments, got %s" % l)
    elif l == 1:
        # Only stop
        start, stop, step = 0, args[0], 1
    else:
        start, stop, step = args[0], args[1], args[2] if l == 3 else 1
    i = start
    while (i < stop if step > 0 else i > stop):
        yield i
        i += step


def enumerate(iterable, *args):
    """
    enumerate(iterable[, start]) -> iterator for index, value of iterable

    Return an enumerate object.  iterable must be another object that supports
    iteration.  The enumerate object yields pairs containing a count (from
    start, which defaults to zero) and a value yielded by the iterable argument.
    enumerate is useful for obtaining an indexed list:
        (0, seq[0]), (1, seq[1]), (2, seq[2]), ...

    """
    if 1 < len(args):
        raise TypeError("enumerate() takes at most 2 arguments (%d given)" %(len(args)))
    start = args[0] if args else 0
    for i, x in builtins.enumerate(iterable):
        yield i+start, x


#IDEA: Harcodear el import para que estos hacks se expandan.

#FIXME: Hacer esto en decorador (decorar la funcion original), mas cool.
str_original_format = str.format
def str_format(self, *args, **kwargs):
    assert(type(self) == str)
    if any([type(a) in proxy.ProxyObject.__subclasses__() for a in args]):
        #TODO: considerar kwargs
        return proxy.StringProxy(self).format(*args, **kwargs)
    else:
        return str_original_format(self, *args, **kwargs)
