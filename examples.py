
import builtins

def simple(i):
    """
    :type: i: builtins.int
    :assume: i > 0
    :ensure: returnv != None
    """
    if i:
        print("i es diferente a 0")
    else:
        print("i es igual a 0")


def simple_bool(a, b):
    """
    :type: a: bool, b: bool
    """
    return a and b


def fun(a):
    """
    :type: a: int
    """
    a = a + 3
    if a:
        raise AssertionError('Chupalaaaa')
    else:
        print('caca')
        return a


def funab(a, b):
    """
    :type: a: int, b: int
    """
    a = a + 3
    if a == b:
        if a != a:
            raise AssertionError('Imposibruuuu')
        raise AssertionError('Chupalaaaa')
    else:
        return a


def funfor(a):
    """
    :type: a: int
    """
    a = a + 3
    for i in range(a):
        if a == 2:
            raise AssertionError('Chupalaaaa')
    return a


def funlist_complicada(a):
    """
    :type: a: int
    """
    l1 = [1,2,3,4,5,a]
    l2 = l1[0:a]
    if len(l2) < 3:
        raise AssertionError('Chupalaaaa')
    else:
        print('caca')
        return a


def funlist_in(l, i, j):
    """
    :type: l: list, i: int, j: int
    """
    if len(l) == i:
        if l[j] == i:
            print("Geniaaal")
        else:
            print("Casii")
    else:
        print("Chauchis")


def funlist_iter(l): # FIXME: Performance issue
    """
    :type: l: list
    :assume: len(l) == 2
    """
    for i, x in enumerate(l):
        print(x)


def funlist_slice(l, start, stop, step):
    """
    :type: l: list, start: int, stop: int, step: int
    """
    return l[start:stop:step]


def assert_not_divisible(x, y):
    """
    :type: x: int, y: int
    """
    if(x > y):
        r = divisible(x, y)
        if r:
            raise Exception("x no tiene que ser divisible por y")
        else:
            print("Coooorreeectooo!")
    else:
        raise Exception("x tiene que ser mas grande que y")


def divisible(x, y):
    """
    :type: x: int, y: int
    :assume: x > y
    """
    if x > y:
        return divisible(x - y, y)
    elif x == y or x == 0:
        return True
    else:
        return False


class Test():
    counter = 0
    def __init__(self, a, l):
        """
        :types: a: int, l:list
        """
        self.a = a
        self.l = l

    def recalculate(self):
        self.counter += 1
        self.a = self.l.__len__()

    def __str__(self):
        return "Test <%s, %s>" % (self.a, self.l)


def test_Test(t):
    """
    :types: t:Test
    """
    if t.a != t.l.__len__():
        t.recalculate()
    else:
        t.l[0] = 5
    return t.l[0]


def test_mock_Test(a, l):
    """
    :types: a:int, l: list
    """
    if a != l.__len__():
        a = l.__len__()
    else:
        l[0] = 5
    return l[0]


def test_gt(a, b):
    """
    :types: a: int, b: int
    """
    if a > b:
        return b - a
    else:
        return a - b


def test_kw_args(a=1, b=2):
    """
    :types: a: int, b: int
    """
    if a > b:
        return True
    else:
        return False


class Simple():
    def __init__(self, a, b):
        """
        :types: a: int, b: int
        """
        if a > b:
            print ("Primero mayor!")
        else:
            print ("Primero NO mayor!")


def simple_test(s):
    """
    :types: s: Simple
    """
    return s


class Queue(object):
    def __init__(self, initial):
        """
        :types: initial: list
        """
        self.queue = []
        if initial:  # TODO: ver que sea iterable
            self.queue = initial #[x for x in initial]

    def push(self, item):
        self.queue.append(item)

    def pop(self):
        try:
            return self.queue.pop()
        except IndexError:
            raise IndexError("Pop from empty Queue")

    def __len__(self):
        return len(self.queue)

    def __repr__(self):
        # import ipdb; ipdb.set_trace()
        # print(type(self.queue), self.queue);import sys;sys.exit()
        return self.queue.__repr__()


def example00(pushes, pops):
    """
    :types: pushes: int, pops: int
    """
    q = Queue([])
    i = 0; j = 0

    while i < pushes:
        q.push(i)
        i += 1
    while j < pops:
        q.pop()
        j += 1
    return q.__len__() > 0  # Tiene elementos?


def example01(queue, pushes, pops):
    """
    :types: queue: Queue, pushes: int, pops: int
    """
    i = 0; j = 0
    while i < pushes:
        queue.push(i)
        i += 1
    while j < pops:
        queue.pop()
        j += 1
    result = len(queue) > 0
    return result  # Tiene elementos?


def example02(a, b):
    """
    :types: a:int, b: int
    """
    return a//b


def example03(list):
    """
    Quicksort using list comprehensions

    :types: list: list
    """
    if not list:
        return []
    else:
        pivot = list[0]
        lesser = example03([x for x in list[1:] if x < pivot])
        greater = example03([x for x in list[1:] if x >= pivot])
        r = lesser + [pivot] + greater
        assert (sorted(r))
        return r


def quicksort(l):
    """
    Quicksort using list comprehensions

    :types: l: list
    :assume: len(l) == 3
    :ensure: sorted(l) == returnv,
    """
    if not l:
        return []
    else:
        pivot = l[0]
        lesser = quicksort([x for x in l[1:] if x < pivot])
        greater = quicksort([x for x in l[1:] if x >= pivot])
        return lesser + [pivot] + greater

def example04(array): #=[12,4,5,6,7,3,1,15]
    """
    :types: array: list
    """
    less = []
    equal = []
    greater = []

    if array.__len__() > 1 and array.__len__() < 5:
        pivot = array[0]
        for x in array:
            if x < pivot:
                less.append(x)
            elif x == pivot:
                equal.append(x)
            else:# x > pivot:
                greater.append(x)
        # Don't forget to return something!
        return example04(less)+equal+example04(greater)  # Just use the + operator to join lists
    # Note that you want equal ^^^^^ not pivot
    else:  # You need to hande the part at the end of the recursion - when you only have one element in your array, just return the array.
        return array


def multitypes(a, b):
    """
    :types: a: [int, list], b: [int, list]
    """
    # import ipdb; ipdb.set_trace()
    a_m = "a es int" if isinstance(a, int) else "a es list"
    b_m = "b es int" if isinstance(b, int) else "b es list"
    print ("%s y %s" % (a_m, b_m))


def test_strings1(s1, s2):
    """
    :types: s1: str, s2: [str]
    """
    result = ""
    if s1 == s2:
        tmp = s1 + s2
        result += tmp
    elif len(s1) < len(s2):
        if s1 > s2:
            result = "%s es mas corto que %s pero es mayor lexicográficamente." % (s1, s2)
        else:
            result = "%s mayor que %s en todo sentido." % (s2, s1)
    else:
        if s1[0] == s1[0]:
            result = "{0} y {1} coniciden en el primer caracter: {2}".format(s1, s2, s1[0])
        elif s1.isdecimal():
            result = "s1 es un número! (y nada que ver con s2)."
    return result


import types


def print_species(tree):
    """
    :type: tree: list
    """
    if type(tree) == list:
        for child in tree:
            print_species(child)
    else:
        print(tree)


class Lista:
    def __init__(self, value, sig):
        """
        :type: value: [int], sig: [Lista, NoneType]
        """
        self.value = value
        self.sig = sig

    def __repr__(self):
        if self.sig == None:
            return repr(self.value)
        else:
            return repr(self.value) + " -> " + repr(self.sig)



ls = Lista(1,Lista(2,None))


def sum_lista(ls, ka=None):
    """
    :type: ls: Lista, ka: int
    """
    if (ls.sig == None):
        return ls.value
    else:
        return ls.value + sum_lista(ls.sig)


class BinaryTree:
    def __init__(self, rootObj, leftChild=None, rightChild=None):
        """
        :type: rootObj: int, leftChild:[BinaryTree, None], rightChild: [BinaryTree, None]
        """
        self.key = rootObj
        self.leftChild = leftChild
        self.rightChild = rightChild

    def __repr__(self):
        return "<" + repr(self.leftChild)  + " ,(" + repr(self.key) + "), " + repr(self.rightChild) + ">"


def insertLeft(t, newNode):
    """
    :type: t: BinaryTree, newNode: int
    :assume: newNode >= 0
    """
    if t.leftChild == None:
        t.leftChild = BinaryTree(newNode)
    else:
        tn = BinaryTree(newNode, leftChild=t.leftChild)
        t.leftChild = tn
    return t


import math

def sum_fac(n):
    """
    :type: n: int
    """
    if n < 0:
        raise ValueError
    facs = [math.factorial(i) for i in range(n+1)]
    return sum(facs)


def sum_fac2(n):
    """
    :type: n: int
    """
    if n < 0:
        raise ValueError
    elif n == 0:
        return 1
    elif n == 1:
        return 2
    elif n == 2:
        return 4
    elif n >= 3:
        return sum_fac2(n-1) * n - sum_fac2(n-3) * (n-1)

def test_sum_fac(n):
    """
    :type: n: int
    """
    return sum_fac2(n) == sum_fac(n)

def fact_spec(x):
    """
    :type: x: int
    """
    n = 1
    while x > 0:
        n *= x
        x -= 1
    return n

def faulty_fact(x):
    """
    :type: x: int
    :ensure: returnv == fact_spec(x)
    """
    if x == 40:
        return 123456789
    n = 1
    while x > 0:
        n *= x
        x -= 1
    return n


def suma_con_bug(a, b):
    """
    :types: a: int, b: int
    """
    result = a + b
    if result == 42:
        print("La gran respuesta.")
    if a - b == 42:
        raise Exception("Bug")
    return result


def f_l_j(l, j):
    """
    :types: l: list, j: int
    """
    l += [1,2,3]
    return l[j]


def make_heap(heap, item):
    """
    :types: heap: list, item: int
    """
    from heapq import heapify, heappop
    heapify(heap)
    return heappop(heap)


def adivina(x) :
    """
    :type: x: int
    """
    import random
    for i in range(0, 1000000):
        random.randint(0, 42)
    if x == random.randint(0, 42):
        print("Adivinaste!")
    else:
        print("La puuucha")

def positive2(ls):
    """
    :type: ls: list
    :assume: len(ls) == 3
    """
    r = True
    for item in ls:
        r = r and item >= 0
    return r

import types

def print_species(tree):
    """
    :type: tree: list
    :assume: len(tree) == 3
    """
    if type(tree) is list:
        for child in tree:
            print_species(child)
    else:
        print(tree)

def f_un_solo_camino (x,y):
    """
    :type: x: int, y: int
    """
    z = 2*y
    if (x == 100000):
        if (x<z):
            raise Exception("Error!")

def margs(a, *args, k=True):
    """
    :types: a: [int, list], k:int
    """
    if args:
        return args[0]
    else:
        return a