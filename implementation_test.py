#! /usr/bin/env python3
#coding=utf-8

import proxy
import sys
from types import FunctionType
import contracts
from contracts import TypeContract
from capture import Capturing
import z3 as _z3
import traceback
try:
    from clint.textui import colored
    # ('red', 'green', 'yellow', 'blue', 'black', 'magenta', 'cyan', 'white')
except ImportError:
    print("`clint` module not found, please run `pip3 install clint`.")
    sys.exit(-1)


def test_class(cls, method=None, show_progress=True, show_statistics=True,
               start_from=None, debug=False):
    passed_tests, failed_tests = 0, 0

    if show_progress or show_statistics:
        print(" > Running automated tests cases for class %s" % colored.blue(cls.__name__))

    # Get list of methods to test
    proxy_class_methods = [x for x in dir(cls) if type(getattr(cls, x)) == FunctionType]
    not_relevant_methods = ["__init__", "__repr__", "__str__", "__hash__"]
    real_class_methods = [x for x in dir(cls.emulated_class)]
    if not method:
        methods_list = [getattr(cls, x) for x in proxy_class_methods\
                   if x in real_class_methods and not x in not_relevant_methods]
    else:
        methods_list = [getattr(cls, method)]
    # Test each method
    skip = False if not start_from else True
    for method in methods_list:
        # Print method name
        if skip and method.__name__ == start_from:
            skip = False
        if skip:
            continue
        try:
            r, e, paths = test_method(cls, method)
        except contracts.ContractError as e:
            failed_tests += 1
            print(colored.red("\n    ContractError: ") + str(e))
            print(status_string(method, False, 0))
            continue
        passed_tests += 1 if r else 0
        failed_tests += 0 if r else 1
        # Print exit status
        if show_progress:
            print(status_string(method, r, paths))
            if not r: print(error_string(e, indent="    ", debug=debug))

    # Statistics
    if show_statistics:
        print(" < Runned %s tests: %s/%s [passed/failed]\n" %\
            (passed_tests + failed_tests, passed_tests, failed_tests))
    return passed_tests, failed_tests


def test_method(cls, method):
    """
    r: if excecuted withot errors
    e: extra info for errors
    """
    r = True
    e = {}
    paths = 0
    real_class = cls.emulated_class
    executions = proxy.explore(method)
    for result in executions:
        args = result['args']
        kwargs = result['kwargs']
        returnv = result['result']
        exception = result['exception']
        outputs = result['outputs']
        paths += 1
        if isinstance(exception, proxy.MaxDepthError):
            continue
        if isinstance(exception, _z3.Z3Exception):
            # It's definitely an internal error, so let it propogate in this case
            raise exception
        real_exception = None
        real_result = None
        try:
            with Capturing() as output:
                real_result = getattr(real_class, method.__name__)(*args, **kwargs)
        except Exception as err:
            real_exception = err
        if real_result != returnv or type(real_exception) != type(exception):
            r = False
            e = {"args": (args, kwargs),
                 "real": (real_result, real_exception),
                 "proxy": (returnv, exception),
                }
            break
    return (r, e, paths)


# Pretty Formatted strings for status and errrors
def error_string(e, indent="    ", debug=False):
    real_exception = e["real"][1] if e["real"][1] else None
    real_exception_tname = type(real_exception).__name__ if real_exception else None
    exception = e["proxy"][1] if e["proxy"][1] else None
    exception_tname = type(exception).__name__ if exception else None
    result = e["proxy"][0]
    real_result = e["real"][0]
    args = e["args"][0]
    kwargs = e["args"][1]
    # Error found! Pretty-Print error
    params = [repr(x) for x in args] + ["%s=%s" %(k, v) for (k,v) in kwargs.items()]
    error_str = colored.red("ERROR FOUND")
    error_params = error_str + ": running with params %s" % (", ".join(params))
    # Get the right error description
    if real_exception and not exception:
        error_description = "Expected exception: %s but no exception was raised" %(real_exception_tname)
    elif real_exception and exception:
        error_description = "Expected exception: %s but ProxyObject raised: %s" %(real_exception_tname, exception_tname)
    elif exception:
        error_description = "Unexpected exception: %s (No exeception should be raised)" %(colored.red(exception_tname))
    else:
        error_description = "Expected result: %s, but ProxyObject returned: %s" %(repr(real_result), repr(result))
    if debug and exception:
        error_description += "\n\n" + repr(exception)
        error_description += "\n"+"\n".join(traceback.format_tb(e["proxy"][1].__traceback__))
    return (indent + error_params + '\n' + indent + error_description).replace('\n', '\n' + indent)


def status_string(method, r, p, indent="    "):
    symbol = colored.green("✔") if r else colored.red("✘")
    return indent + symbol + "  " + method.__name__ + "\t[paths: %s]" % p


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("cls", action="store", nargs="?",
                        help="Run tests for class name `cls'")
    parser.add_argument("-m", "--method", action="store",
                        help="Run tests for specific method in class `cls'")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="Show more debug info for exceptions raised")
    parser.add_argument("-f", "--fromm", action="store",
                        help="skip tests until method with given name is found")

    args = parser.parse_args()
    classes = [x.__name__ for x in proxy.ProxyObject.__subclasses__()] if\
                                                not args.cls else [args.cls]
    passed_tests, failed_tests = 0, 0
    for c in classes:
        cls = getattr(proxy, c, None)
        if not cls:
            print(colored.red("\nWARNING") + ": class %s not found\n" % c)
        elif proxy.ProxyObject not in cls.__bases__:
            print(colored.red("\nWARNING") + ": class %s does not inherit from ProxyObject\n" % c)
        else:
            try:
                p, f = test_class(cls, method=args.method, debug=args.debug, start_from=args.fromm)
                passed_tests += p
                failed_tests += f
            except Exception as e:
                print(colored.red("\nINTERNAL ERROR") + ": %s" % e)
                raise e
