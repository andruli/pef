import bdb
import inspect
import ast
import proxy


class Visitor(ast.NodeVisitor):

    def generic_visit(self, node):
        """Called if no explicit visitor function exists for a node."""
        result = []
        for field, value in ast.iter_fields(node):
            if isinstance(value, list):
                for item in value:
                    if isinstance(item, ast.AST):
                        result.extend(self.visit(item))
            elif isinstance(value, ast.AST):
                result.extend(self.visit(value))
        return result

    def visit_Assign(self, stmt):
        """
        Concrete variables convertion into symbolic ones.
        After a = value, we do a = Symbolic(value)
        In order to do that, we stop at the next frame, when the variable is
        created and under the local scope.
        """
        result = []
        # Here new variable creation happens
        for target in stmt.targets:
            if isinstance(target, ast.Name):
                result.append(target.id)
                # if target.id == 'result':
                    # print (dir(stmt.value.left))
                    # print (stmt.value.left.s)
            else:
                result.extend(AssignVisitor().visit(target))
        return result


class AssignVisitor(Visitor):
    """
    For visiting inside a Assign node and get varnames
    """
    def visit_Name(self, stmt):
        return [stmt.id]


class Interceptor(bdb.Bdb):
    # def user_call(self, frame, args):
    #     print(frame.f_code.co_name)
    def __init__(self, *args, **kwargs):
        self.varnames_to_convert = []
        super().__init__(*args, **kwargs)


    def user_line(self, frame):
        if self.varnames_to_convert:
            self.symbolize_scope(frame)
        code_line = inspect.getframeinfo(frame).code_context[0].strip()  # str
        # print(code_line)
        # if 'result' in frame.f_locals:
        #     print(frame.f_locals)
        try:
            self.varnames_to_convert = Visitor().visit(ast.parse(code_line))
        except SyntaxError:
            self.varnames_to_convert = []
            # print("Syntax error in this Line: ", code_line)
            frame.f_locals  # Sin esta linea se shompe.. misterioso.
        # print("result: ", self.varnames_to_convert)

    def symbolize_scope(self, frame):
        local_scope = frame.f_locals
        for varname in self.varnames_to_convert:
            if isinstance(varname, str):
                if varname == 'result':
                    print("result will appear in next scope:")

            # try:
            #     local_scope[varname] = proxy.make_symbolic(local_scope[varname])
            # except:
            #     print(local_scope)


