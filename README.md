# USE

    ./pef <program_file>:<function_name>

Para más información: 

    ./pef -h


#INSTALATION

* Instalación de Z3 en un virtualenv

        mkvirtualenv -p /usr/bin/python3.3 pef # Nuevo virtualenv con python3.3

* Instalar z3:

        cd z3;
        python3 scripts/mk_make.py;
        cd build;
        make;
        make install;

* Instalar requerimientos:

        pip install -r requeriments.txt

* Para autocompletado:

      source _pef-bash-completion.sh

  o copiarlo a la carpeta /etc/bash_completion.d/ para hacerlo permanente


## KNOWN ERRORS

* Error al hacer python3 scripts/mk_make.py:

    Buscar versión de Z3 con soporte para python3 
    (algunos commits rompen la compatibilidad)

    Código en https://git01.codeplex.com/z3

* Errores con git:

    hay que actualizar la versión:

        git clone https://github.com/git/git.git 
        sudo apt-get install gettext zlib1g-dev asciidoc libcurl4-openssl-dev 
        cd git
        make configure 
        ./configure --prefix=/usr
        make all doc
        sudo make install