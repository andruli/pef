class ClassCreator(object):

    def __init__(self, indent='    '):
        self.indent = indent

    def proxyfy(self, cls):
        cls_name = cls.__name__
        cls_methods = dir(cls)
        cls_doc = self._proxy_docstring(cls)
        cls_definition = "class %sProxy(ProxyObject):" % cls.__name__.capitalize()
        methods = []
        # Create all methods definition strings
        for m in cls_methods:
            m = getattr(cls, m)
            if callable(m):
                methods.append(self._proxy_method(m))
        # Add indentation to methods
        methods = '\n\n'.join([self._indent_string(x) for x in methods])
        return '\n'.join([cls_definition, cls_doc, methods])

    def _proxy_method(self, method):
        method_name = method.__name__
        method_doc = self._proxy_docstring(method)
        method_args = ["self", "*args", "**kwargs"]
        method_definition = "def %s(%s):" %(method_name, ", ".join(method_args))
        method_body = self._indent_string('raise NotImplementedError("%s")' % method_name)
        return method_definition + '\n' + method_doc + '\n' + method_body

    def _proxy_docstring(self, object):
        doc = object.__doc__ if object.__doc__ else "TODO: complete docstring"
        return self._indent_string("".join(['"""\n', doc , '\n"""']))

    def _indent_string(self, string):
        return self.indent + string.replace('\n', '\n' + self.indent)

