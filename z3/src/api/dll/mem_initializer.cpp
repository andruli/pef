// Automatically generated file.
#include"gparams.h"
#include"prime_generator.h"
#include"trace.h"
#include"debug.h"
#include"rational.h"
#include"symbol.h"
void mem_initialize() {
rational::initialize();
initialize_symbols();
gparams::init();
}
void mem_finalize() {
gparams::finalize();
prime_iterator::finalize();
finalize_trace();
finalize_debug();
rational::finalize();
finalize_symbols();
}
