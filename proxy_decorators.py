from decorator import decorator
from functools import wraps
import inspect


# Usefull Decorators
# FIXME: this function does not work for the following cases:
#  *) Function call using kw for not kwargs.
#       def f(a,b): pass
#       f(a=1, b=2)
def handle_varargs_and_defaults(*opt_defaults):
    opt_defaults = [x for x in opt_defaults]
    def wrapper(fun):
        posargs, varargs, _, defaults, _, _, _ = inspect.getfullargspec(fun)
        if defaults != None:
            posargs = posargs[:len(posargs)-len(defaults)]
            for x in defaults[::-1]:
                opt_defaults.insert(0, x)
        # Remove defaulted args
        posargs_min, posargs_max = len(posargs), len(posargs) + len(opt_defaults)
        def fun_with_len_check(fun, *args, **kwargs):
            if varargs and len(args) >= posargs_min:
                opt_args = args[posargs_min:]
                if len(args) > posargs_max:
                    fun_name, args_given = fun.__name__, len(args)
                    args_expected = posargs_max
                    raise TypeError("%s() takes at most %d argument%s (%d given)"\
                                     %(fun_name, args_expected,\
                                  '' if args_expected == 1 else 's',args_given))
                else:
                    args = [x for x in args]
                    args.extend(opt_defaults[len(opt_args):])
            return fun(*args, **kwargs)
        return decorator(fun_with_len_check)(fun)
    return wrapper


def check_comparable_types(comp_fun):
    @wraps(comp_fun)
    def comp(self, other):
        stype = type(self)
        otype = type(other)
        rtype = self.emulated_class
        if otype not in [stype, rtype]:
            raise TypeError("unorderable types: %s(), %s()" %(stype.__name__,\
                                                              otype.__name__))
        else:
            return comp_fun(self, other)
    return comp


def check_equality(eq_fun):
    @wraps(eq_fun)
    def eq(self, other):
        stype = type(self)
        otype = type(other)
        rtype = self.emulated_class
        if otype not in [stype, rtype]:
            return False
        else:
            return eq_fun(self, other)
    return eq


def forward_to_rfun(*args):
    def forward_bin_fun_to_rfun(fun):
        """
        Forward a call to fun the its reverse version if necessary.
        Example: 10 * [1,2,3] ----> [1,2,3] * 10
        """
        def valid_parameter(self, other):
            if args:
                return any([isinstance(other, x) for x in args])
            else:
                return any([isinstance(other, x) for x in [type(self), self.emulated_class]])
        def forwarder_fun(fun, self, other):
            if not valid_parameter(self, other):
                # This is a bit hardcoded
                rfun_name = "__r" + fun.__name__[2:]
                rfun = getattr(other, rfun_name, None)
                if rfun:
                    return rfun(self)
                else:
                    raise TypeError("unsupported operand type(s) for %s: '%s' and '%s'"\
                        %(fun.__name__, type(self).__name__, type(other).__name__))
            else:
                return fun(self, other)
        return decorator(forwarder_fun)(fun)
    return forward_bin_fun_to_rfun

def check_self_and_other_have_same_type(fun):
    """
    This function/decorator is usefull in __rfuns__. It checks that the type of
    other is either the type of self or self.emulated_class
    """
    def type_check(fun, self, other):
        if not (isinstance(other, type(self)) or isinstance(other, self.emulated_class)):
            raise TypeError("unsupported operand type(s) for %s: '%s' and '%s'"\
                     %(fun.__name__, type(self).__name__, type(other).__name__))
        else:
            return fun(self, other)
    return decorator(type_check)(fun)