def fun(x):
    if x > 3:
        return x          # PC = [x > 3]
    else:
        x = 1 + x         # PC = [not x > 3, x = 1 + x]
        if x <= 3:
            return x      # PC = [not x > 3, x = 1 + x, x <= 3]
        error()           # PC = [not x > 3, x = 1 + x, not x <= 3]