class IntProxy(ProxyObject):
    def __init__(self, term=None):
        if term != None:
            self.term = term
        else:
            self.term = fresh_symbolic_var("int")

    def __add__(self, other):
        other = other if type(other) == int else other.term
        return IntProxy(self.term + other)

    def __bool__(self):
        return self != 0

    def __eq__(self, other):
        other = other if type(other) == int else other.term
        return BoolProxy(self.term == other)

    def __ne__(self, other):
        return not self.__eq__(other)