# Inicial
def fun_exceptions (x):
    l = [1,2,3]
    if x < 3:
        return l[x]  # posible IndexError
    elif x > 3:
        raise MyError("x debe ser menor a 3")
    else:
        # Branch poco visitado
        return no_definida(x)  # NameError


# Introducir contratos
def fun_exceptions (x):
    """
    assume: isinstance(x, number)
    raises: MyError,
    ensure: return != None
    """
    l = [1,2,3]
    if x < 3:
        return l[x]  # posible IndexError
    elif x > 3:
        raise MyError("x debe ser menor a 3")


# Ejecución simbólica
def fun(x):
    if x > 3:
        return x          # PC = [x > 3]
    else:
        x = 1 + x         # PC = [not x > 3, x = 1 + x]
        if x <= 3:
            return x          # PC = [not x > 3, x = 1 + x, x <= 3]
        error()       # PC = [not x > 3, x = 1 + x, not x <= 3]

