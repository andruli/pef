def fun_exceptions (x):
    l = [1,2,3]
    if x < 3:
        return l[x]  # posible IndexError
    elif x > 3:
        raise MyError("x debe ser menor a 3")
    else:
        # Branch poco visitado
        return no_definida(x)  # NameError