def fun_exceptions (x):
    """
    assume: isinstance(x, number)
    raises: MyError,
    ensure: returnv != None
    """
    l = [1,2,3]
    if x < 3:
        return l[x]  # posible IndexError
    elif x > 3:
        raise MyError("x debe ser menor a 3")

def abs (x):
    """
    assume: isinstance(x, number)
    ensure: returnv >= 0, returnv == x | returnv == -x
    """
    if x < 0:
        return -x
    if x > 0:
        return x