def foo(i, l):
    """
    assume:  isinstance(l, list) and isinstance(i, int)
    ensures: result < 2
    raises:  []
    """
    result = 0
    if l[i] < 2:
        result = l[i]
    else:
        result = None
    return result

def swap(x, y):
    tmp = y[0]
    y[0] = x[0]
    x[0] = tmp
    return None