def abs (x):
    """
    abs (x) = |x|
    """
    if x < 0:
        return -x
    if x > 0:
        return x