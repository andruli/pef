#! /usr/bin/env python3
#coding=utf-8

if __name__ == '__main__':
    import importlib
    import argparse
    import proxy
    import time
    import traceback
    try:
        from clint.textui import colored
    except ImportError:
        print("`clint` module not found, please run `pip3 install clint`.")
        sys.exit(-1)
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("file_and_function", action="store",
                        help="Syntax: <program_file>:<function_name>")
    parser.add_argument("-d", "--depth", action="store", default=10,
                        help="Depth limit")
    parser.add_argument("-n", "--nexp", action="store", default=5,
                        help="Depth limit")
    parser.add_argument("-D", "--debug", action="store_true",
                        help="Show more info")
    parser.add_argument("-s", "--show-output", action="store_true",
                        help="Run tests")

    import _builtins
    parsed_args = parser.parse_args()
    module_name, function_name = parsed_args.file_and_function.split(':')
    module = importlib.import_module(module_name.strip().split(".")[0])
    function = getattr(module, function_name.strip())
    # It seams to be necessary to save and restablish the builtins attribute
    # before quiting
    original_bultins =  module.__builtins__
    module.__builtins__ = _builtins
    proxy.ProxyObject.max_depth = int(parsed_args.depth)
    # TOMAR TIEMPO ACA
    s_time = 0
    s_max_time = 0
    c_time = 0
    exp_times = int(parsed_args.nexp)
    for n in range(exp_times):
        print("Running execution number: %d" %(n + 1))
        explored = 0
        all_args = []
        executed_paths = 0
        max_pathcondition_lenght = 0
        start_time = time.process_time()
        r = proxy.explore(function)
        for x in r:
            executed_paths += 1
            args, kwargs, ret_value, exception, outputs, paths, pathconditions = x
            all_args.append(args)
            max_pathcondition_lenght = max(max_pathcondition_lenght, len(pathconditions))
            if isinstance(exception, proxy.MaxDepthError):
                continue
            explored += 1
            elapsed_time = time.process_time() - start_time
            s_max_time = max(s_max_time, elapsed_time)
            s_time += elapsed_time
            start_time = time.process_time()

        start_time = time.process_time()
        for args in all_args:
            try:
                function(*args)
            except:
                pass
        elapsed_time = time.process_time() - start_time
        c_time += elapsed_time
    s_time /= exp_times
    c_time /= exp_times
    s_path_time = s_time / executed_paths


    print ("Tiempo promedio de %d ejecuciones (simbólico): %f" % (exp_times, s_time))
    print ("Tiempo promedio de %d ejecuciones (concreto): %f" % (exp_times, c_time))
    print ("# # # #")
    print ("Tiempo promedio por camino (%d caminos) de %d ejecuciones (simbólico): %f" % (executed_paths, exp_times, s_path_time))
    print ("Tiempo máximo camino %f" % s_max_time)
    print ("Caminos explorados: %d, Completos/Parciales = %d/%d" %(executed_paths, explored, executed_paths-explored))
    print ("Máxima profundidad alcanzada: %d" %(max_pathcondition_lenght))
    module.__builtins__ = original_bultins