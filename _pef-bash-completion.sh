# bash completion for path_executor.py ;)

_pef_get_functions()
{
# $1: module
# $2: class.class.class.method.function
python3 -c "
from importlib import import_module
try:
    m = import_module('$1')
    function_path = '$2'.split('.')
    for o in function_path[:-1]:
        m = getattr(m, o)
    path = function_path[:-1]
    d = ['.'.join(path+[d]) for d in dir(m)]
    print(' '.join(d))
except Exception:
    pass"
}

_pef()
{
    local prev cur opts dirname filename to_execute module function methods files
    opts=""
    COMPREPLY=()
    _get_comp_words_by_ref cur prev

    case "$cur" in
        *.py:*)
            to_execute=(${cur//:/ })
            # Get current filename and path
            filename=$(basename "${to_execute[0]}")
            dirname=$(dirname "${to_execute[0]}")
            # Strip extension and filename
            # extension="${filename##*.}"
            module="${filename%.*}"
            function=${to_execute[1]}
            cwd=$pwd
            cd "$dirname"
            methods=( $( _pef_get_functions $module $function ) )
            cd "$cwd"
            COMPREPLY=( $( compgen -W "${methods[*]}" -- "$function" ) )
            ;;
        *.py)
            if [ -f "$cur" ]; then
                COMPREPLY=( $( echo "$cur:" ) )
            fi
            ;;
        *)
            dirname=$(dirname "$cur")
            if [ -d "$dirname" ]; then
                files=( $( for x in $(ls "$dirname"); do
                               if [[ "$x" == *".py" ]] || [[ -d  "$x" ]]; then
                                   echo $x
                               fi
                           done ) )
                COMPREPLY=( $( compgen -W "${files[*]}" -- "$cur" ) )
            fi
    esac
    return 0
}

complete -o nospace -F _pef pef.py