import subprocess

class git():
    #TODO: get this commands dinamically
    commands = ['add', 'add--interactive', 'am', 'annotate', 'apply',
                'archimport', 'archive', 'bisect', 'bisect--helper', 'blame',
                'branch', 'bundle', 'cat-file', 'check-attr', 'check-ignore',
                'check-mailmap', 'check-ref-format', 'checkout', 'checkout-index',
                'cherry', 'cherry-pick', 'citool', 'clean', 'clone', 'column',
                'commit', 'commit-tree', 'config', 'count-objects', 'credential',
                'credential-cache', 'credential-cache--daemon',
                'credential-osxkeychain', 'credential-store', 'cvsexportcommit',
                'cvsimport', 'cvsserver', 'daemon', 'describe', 'diff',
                'diff-files', 'diff-index', 'diff-tree', 'difftool',
                'difftool--helper', 'fast-export', 'fast-import', 'fetch',
                'fetch-pack', 'filter-branch', 'fmt-merge-msg', 'for-each-ref',
                'format-patch', 'fsck', 'fsck-objects', 'gc', 'get-tar-commit-id',
                'grep', 'gui--askpass', 'hash-object', 'help', 'http-backend',
                'http-fetch', 'http-push', 'imap-send', 'index-pack', 'init',
                'init-db', 'instaweb', 'log', 'lost-found', 'ls-files',
                'ls-remote', 'ls-tree', 'mailinfo', 'mailsplit', 'merge',
                'merge-base', 'merge-file', 'merge-index', 'merge-octopus',
                'merge-one-file', 'merge-ours', 'merge-recursive', 'merge-resolve',
                'merge-subtree', 'merge-tree', 'mergetool', 'mktag', 'mktree',
                'mv', 'name-rev', 'notes', 'p4', 'pack-objects', 'pack-redundant',
                'pack-refs', 'patch-id', 'peek-remote', 'prune', 'prune-packed',
                'pull', 'push', 'quiltimport', 'read-tree', 'rebase', 'receive-pack',
                'reflog', 'relink', 'remote', 'remote-ext', 'remote-fd', 'remote-ftp',
                'remote-ftps', 'remote-http', 'remote-https', 'remote-testsvn',
                'repack', 'replace', 'repo-config', 'request-pull', 'rerere',
                'reset', 'rev-list', 'rev-parse', 'revert', 'rm', 'send-email',
                'send-pack', 'sh-i18n--envsubst', 'shell', 'shortlog', 'show',
                'show-branch', 'show-index', 'show-ref', 'stage', 'stash', 'status',
                'stripspace', 'submodule', 'subtree', 'svn', 'symbolic-ref', 'tag',
                'tar-tree', 'unpack-file', 'unpack-objects', 'update-index',
                'update-ref', 'update-server-info', 'upload-archive', 'upload-pack',
                'var', 'verify-pack', 'verify-tag', 'web--browse', 'whatchanged',
                'write-tree']

    def __dir__(self):
        return super().__dir__() + [x.replace('-', '_') for x in self.commands]

    def __getattr__(self, attr):
        if attr in [x.replace('-', '_') for x in self.commands]:
            command = attr.replace('_', '-')
            doc = subprocess.check_output(("git help %s" % command).split()).decode("utf8")
            def virtual_attr(*args, **kwargs):
                args = " ".join(args)
                kwargs = " ".join("%x=%x" %(x,kwargs[x]) for x in kwargs.items())
                full_command = "git %s %s %s" % (command, args, kwargs)
                o = subprocess.check_output(full_command.split()).strip()
                return o.decode("utf8")
            virtual_attr.__doc__ = doc
            return virtual_attr
        else:
            return getattr(super(), attr)
